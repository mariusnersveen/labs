#ifndef SHADER_H_
#define SHADER_H_

#include <string>
#include <glad/glad.h>
#include <glm/glm.hpp>

class Shader
{
private:
	GLuint VertexShader;
	GLuint FragmentShader;
	GLuint ShaderProgram;

	void CompileShader(GLenum shaderType, const std::string& shaderSrc);

public:
	Shader(const std::string& vertexShaderSrc, const std::string& fragmentShaderSrc);
	~Shader();

	void Bind() const;
	void Unbind() const;
	void UploadUniformFloat2(const std::string& name, const glm::vec2& vector);
	void UploadUniformFloat4(const std::string& name, const glm::vec4& vector);
};

#endif // SHADER_H_
