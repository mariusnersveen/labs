#ifndef BUFFERLAYOUT_H_
#define BUFFERLAYOUT_H_

#include <string>
#include <vector>
#include "ShadersDataTypes.h"

struct BufferAttribute
{
	GLint Size;
	ShaderDataType Type;
	GLboolean Normalized;
	GLsizei Offset;
	std::string Name;

	// initialize data type, name, ifnormalize for glVertexAttribPointer
	BufferAttribute(ShaderDataType type, const std::string& name, GLboolean normalized = false)
		: Name(name), Size(ShaderDataTypeSize(type)), Type(type), Offset(0), Normalized(normalized) {}
};

class BufferLayout
{
private:
	std::vector<BufferAttribute> Attributes;
	GLsizei Stride;

	void CalculateOffsetAndStride() {
		GLsizei offset = 0;
		for (auto& attribute : Attributes) {
			attribute.Offset = offset;
			offset += attribute.Size;
		}
		this->Stride = offset;
	}

public:
	BufferLayout() : Attributes(), Stride() {}
	// recieve BufferAttributes, initialize Stride and Attributes of BufferLayout 
	BufferLayout(const std::initializer_list<BufferAttribute>& attributes)
		: Attributes(attributes), Stride() {
		this->CalculateOffsetAndStride();
	}

	inline const std::vector<BufferAttribute>& GetAttributes() const { return this->Attributes; }
	// why this is not const?
	inline GLsizei GetStride() const { return this->Stride; }

	// what is the difference between const_iterator and the other?
	std::vector<BufferAttribute>::iterator begin() { return this->Attributes.begin(); }
	std::vector<BufferAttribute>::iterator end() { return this->Attributes.end(); }
	std::vector<BufferAttribute>::const_iterator begin() const { return this->Attributes.begin(); }
	std::vector<BufferAttribute>::const_iterator end() const { return this->Attributes.end(); }

};

#endif // BUFFERLAYOUT_H_
