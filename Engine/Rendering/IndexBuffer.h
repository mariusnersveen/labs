#ifndef INDEXBUFFER_H_
#define INDEXBUFFER_H_

#include <glad/glad.h>

// an index buffer is equal to an element buffer object
class IndexBuffer
{
private:
	GLuint IndexBufferID;
	GLuint Count;
public:
	// Constructor. It initializes with a data buffer and the size of it.
	// Note that the buffer will be bound on construction.
	IndexBuffer(const void* data, GLsizei count);
	~IndexBuffer();

	// Bind the index buffer
	void Bind() const;
	// Unbind the index buffer
	void Unbind() const;
	// Get the number of elements
	inline GLuint GetCount() const { return Count; }
};

#endif // INDEXBUFFER_H_
