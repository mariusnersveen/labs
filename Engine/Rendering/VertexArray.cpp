#include "VertexArray.h"

VertexArray::VertexArray()
{
	glGenVertexArrays(1, &VertexArrayID);
	glBindVertexArray(VertexArrayID);
}

VertexArray::~VertexArray()
{
	glDeleteVertexArrays(1, &VertexArrayID);
}

void VertexArray::Bind() const
{
	glBindVertexArray(VertexArrayID);
}

void VertexArray::Unbind() const
{
	glBindVertexArray(0);
}

void VertexArray::AddVertexBuffer(const std::shared_ptr<VertexBuffer>& vertexBuffer)
{
	glBindVertexArray(VertexArrayID);
	VertexBuffers.push_back(vertexBuffer);
	vertexBuffer->Bind();
	const auto& layout = vertexBuffer->GetLayout();
	const auto& attributes = layout.GetAttributes();
	for (const auto& attribute : attributes) {
		glVertexAttribPointer(i, ShaderDataTypeComponentCount(attribute.Type), ShaderDataTypeToOpenGLBaseType(attribute.Type),
			attribute.Normalized, layout.GetStride(), (const void*)attribute.Offset);
		glEnableVertexAttribArray(i);
		i++;
	}
}

void VertexArray::SetIndexBuffer(const std::shared_ptr<IndexBuffer>& indexBuffer)
{
	glBindVertexArray(VertexArrayID);
	IdxBuffer = indexBuffer;
	indexBuffer->Bind();
}
