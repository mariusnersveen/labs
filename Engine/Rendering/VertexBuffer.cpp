#include <VertexBuffer.h>
#include <GLFW/glfw3.h>

VertexBuffer::VertexBuffer(const void* data, GLsizei size)
{
	glGenBuffers(1, &VertexBufferID);
	glBindBuffer(GL_ARRAY_BUFFER, VertexBufferID);
	glBufferData(GL_ARRAY_BUFFER, size, data, GL_DYNAMIC_DRAW);
}

VertexBuffer::~VertexBuffer()
{
	glDeleteBuffers(1, &VertexBufferID);
}

void VertexBuffer::Bind() const
{
	glBindBuffer(GL_ARRAY_BUFFER, VertexBufferID);
}

void VertexBuffer::Unbind() const
{
	glBindBuffer(GL_ARRAY_BUFFER, 0);
}
void VertexBuffer::BufferSubData(GLintptr offset, GLsizeiptr size, const void* data) const
{
	glBindBuffer(GL_ARRAY_BUFFER, VertexBufferID);
	glBufferSubData(GL_ARRAY_BUFFER, offset, size, data);
}
