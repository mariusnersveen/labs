#ifndef VERTEXARRAY_H_
#define VERTEXARRAY_H_

#include <memory>
#include <glad/glad.h>
#include "VertexBuffer.h"
#include "IndexBuffer.h"
#include "BufferLayout.h"

class VertexArray
{
private:
	GLuint VertexArrayID;
	std::vector<std::shared_ptr<VertexBuffer>> VertexBuffers;
	std::shared_ptr<IndexBuffer> IdxBuffer;
	// index of vertex attribute
	int i = 0;
	// Get vertex buffers
	const std::vector<std::shared_ptr<VertexBuffer>>& GetVertexBuffers() { return VertexBuffers; }

public:
	// Constructor
	VertexArray();
	~VertexArray();

	// Bind vertex array
	void Bind() const;
	// Unbind vertex array
	void Unbind() const;

	// Add vertex buffer. This method utilizes the BufferLayout internal to
	// the vertex buffer to set up the vertex attributes. Notice that this 
	// function opens for the definition of several vertex buffers.
	void AddVertexBuffer(const std::shared_ptr<VertexBuffer>& vertexBuffer);
	// Set index buffer
	void SetIndexBuffer(const std::shared_ptr<IndexBuffer>& indexBuffer);

	// Get index buffer
	const std::shared_ptr<IndexBuffer>& GetIndexBuffer() { return IdxBuffer; }

};

#endif // VERTEXARRAY_H_
