#ifndef VERTEXBUFFER_H_
#define VERTEXBUFFER_H_

#include <glad/glad.h>
#include "BufferLayout.h"

class VertexBuffer
{
private:
	GLuint VertexBufferID;
	BufferLayout Layout;
public:
	// Constructor. It initializes with a data buffer and the size of it.
	// Note that the buffer will be bound on construction.
	VertexBuffer(const void* data, GLsizei size);
	~VertexBuffer();

	// Bind the vertex buffer
	void Bind() const;
	// Unbind the vertex buffer
	void Unbind() const;
	// Fill out a specific segment of the buffer given by an offset and a size.
	void BufferSubData(GLintptr offset, GLsizeiptr size, const void* data) const;

	// Set buffer layout
	void SetLayout(const BufferLayout& layout) { Layout = layout; }
	const BufferLayout& GetLayout() const { return Layout; }

};

#endif // VERTEXBUFFER_H_
