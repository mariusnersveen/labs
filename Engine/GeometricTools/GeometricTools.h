#include <array>

namespace GeometricTools
{

	constexpr std::array<float, 3 * 2> UnitTriangle2D = 
	{ 
		-0.5f, -0.5f, 
		 0.5f, -0.5f, 
		 0.0f,  0.5f 
	};

	constexpr std::array<float, 6 * 2> UnitSquare2D = 
	{
		-0.5f,  0.5f,
		 0.5f,  0.5f,
		 0.5f, -0.5f,
		-0.5f,  0.5f,
		-0.5f, -0.5f,
		 0.5f, -0.5f 
	};


	// receive devision number, return an array of grids
	template <int divNumX, int divNumY> struct UnitGrid2D {
		std::array<float, divNumX * divNumY * 2> grids;

		constexpr UnitGrid2D() : grids() {
			static_assert(divNumX > 1 && divNumY > 1);
			float gridSizeX = (float)1 / (divNumX - 1);
			float gridSizeY = (float)1 / (divNumY - 1);
			for (int x = 0; x < divNumX-1; x++) {
				for (int y = 0; y < divNumY-1; y++) {
					grids[(divNumX * y + x) * 2] = -(float)0.5 + x * gridSizeX;
					grids[(divNumX * y + x) * 2 + 1] = (float)0.5 - y * gridSizeY;
				}
			}
			// make sure the edges are 1
			for (int x = 0; x < divNumX-1; x++) {
				grids[(divNumX * (divNumY - 1) + x) * 2] = -(float)0.5 + x * gridSizeX;
				grids[(divNumX * (divNumY - 1) + x) * 2 + 1] = -(float)0.5;
			}
			for (int y = 0; y < divNumY-1; y++) {
				grids[(divNumX * y + divNumX - 1) * 2] = (float)0.5;
				grids[(divNumX * y + divNumX - 1) * 2 + 1] = (float)0.5 - y * gridSizeY;
			}
			grids[divNumX * divNumY * 2 - 2] = (float)0.5;
			grids[divNumX * divNumY * 2 - 1] = -(float)0.5;
		}

		// return indices that can fill out element buffer objects(EBO)
		std::array<int, (divNumX - 1) * (divNumY - 1) * 3> UnitGridTopologyTriangles() {
			std::array<int, (divNumX - 1)* (divNumY - 1) * 3 > indecies;
			int at = 0;
			for (int y = 0; y < divNumY - 1; y++) {
				for (int x = 0; x < divNumX - 1; x++) {
					indecies[at] = divNumX * y + x;
					at++;
					indecies[at] = divNumX * (y + 1) + x;
					at++;
					indecies[at] = divNumX * (y + 1) + x + 1;
					at++;
				}
			}
			return indecies;
		}

		std::array<int, (divNumX - 1)* (divNumY - 1) * 3> UnitGridTopologyCheck1() {
			std::array<int, (divNumX - 1)* (divNumY - 1) * 3 > indecies;
			int at = 0;
			std::array<int, 2> evenodd = { 0, 1 };
			for (int y = 0; y < divNumY - 1; y++) {
				for (int x = 0; x < divNumX - 1; x+=2) {
					int idx = y % 2;
					indecies[at++] = evenodd[idx] + divNumX * y + x;
					indecies[at++] = evenodd[idx] + divNumX * (y + 1) + x;
					indecies[at++] = evenodd[idx] + divNumX * (y + 1) + x + 1;

					indecies[at++] = evenodd[idx] + divNumX * y + x;
					indecies[at++] = evenodd[idx] + divNumX * y + x + 1;
					indecies[at++] = evenodd[idx] + divNumX * (y + 1) + x + 1;
				}
			}
			return indecies;
		}
		std::array<int, (divNumX - 1)* (divNumY - 1) * 3> UnitGridTopologyCheck2() {
			std::array<int, (divNumX - 1)* (divNumY - 1) * 3 > indecies;
			int at = 0;
			std::array<int, 2> evenodd = { 1, 0 };
			for (int y = 0; y < divNumY - 1; y++) {
				for (int x = 0; x < divNumX - 1; x+=2) {
					int idx = y % 2;
					indecies[at++] = evenodd[idx] + divNumX * y + x;
					indecies[at++] = evenodd[idx] + divNumX * (y + 1) + x;
					indecies[at++] = evenodd[idx] + divNumX * (y + 1) + x + 1;

					indecies[at++] = evenodd[idx] + divNumX * y + x;
					indecies[at++] = evenodd[idx] + divNumX * y + x + 1;
					indecies[at++] = evenodd[idx] + divNumX * (y + 1) + x + 1;
				}
			}
			return indecies;
		}
	};

	template <> struct UnitGrid2D<1,1> {
		std::array<float, 1 * 1 * 2> grids = {0 ,0};
	};

	template <int divNumX> struct UnitGrid2D<divNumX, 1> {
		std::array<float, divNumX * 1 * 2> grids;
		constexpr UnitGrid2D<divNumX, 1>() {
			float gridSizeX = (float)1 / (divNumX - 1);
			for (int x = 0; x < divNumX; x++) {
				grids[x * 2] = -(float)0.5 + x * gridSizeX;
				grids[x * 2 + 1] = 0;
			}
		}
	};

	template <int divNumY> struct UnitGrid2D<1, divNumY> {
		std::array<float, 1 * divNumY * 2> grids;
		constexpr UnitGrid2D<1, divNumY>() {
			float gridSizeY = (float)1 / (divNumY - 1);
			for (int y = 0; y < divNumY; y++) {
				grids[y * 2] = 0;
				grids[y * 2 + 1] = (float)0.5 - y * gridSizeY;
			}
		}
	};

}