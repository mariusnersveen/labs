#include <iostream>
#include <tclap/CmdLine.h>
#include <glad/glad.h>
#include "GLFWApplication.h"

void GLAPIENTRY MessageCallback(
	GLenum source,
	GLenum type,
	GLuint id,
	GLenum severity,
	GLsizei length,
	const GLchar* message,
	const void* userParam)
{
	std::cerr << "GL CALLBACK:" << (type == GL_DEBUG_TYPE_ERROR ? "** GL ERROR **" : "") <<
		"type = 0x" << type <<
		", severity = 0x" << severity <<
		", message = " << message << "\n";
}

GLFWApplication::GLFWApplication(const std::string& name_, const std::string& version_)
	: name(name_), version(version_), width(600), height(600), window() {}

GLFWApplication::~GLFWApplication() {}

// Argument parsing
unsigned int GLFWApplication::ParseArguments(int argc, char** argv)
{
	try {
		// cmd(printed last in the help text, delimiter, version number, disable default arguments)
		TCLAP::CmdLine cmd("Command description message", ' ', "0.9", false);

		TCLAP::ValueArg<int> widthArg("w", "width", "configure the width of the window", false, 600, "int");
		TCLAP::ValueArg<int> heightArg("h", "height", "configure the height of the window", false, 600, "int");

		cmd.add(widthArg);
		cmd.add(heightArg);

		// Parse the argv array.
		cmd.parse(argc, argv);

		width = widthArg.getValue();
		height = heightArg.getValue();
	}
	catch (TCLAP::ArgException& e) {
		std::cerr << "error: " << e.error() << " for arg " << e.argId() << std::endl;
		return 1;
	}
	return 0;
}


// Initialization
unsigned int GLFWApplication::Init()
{
	// GLFW initialization
	if (!glfwInit()) {
		std::cout << "Failed to initialize GLFW" << std::endl;
		glfwTerminate();
		return EXIT_FAILURE;
	}

	// OpenGL initialization
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 6);
	glfwWindowHint(GLFW_OPENGL_DEBUG_CONTEXT, true);

	window = glfwCreateWindow(width, height, "HelloOpenGL", NULL, NULL);
	if (window == NULL) {
		std::cout << "Failed to create GLFW window" << std::endl;
		glfwTerminate();
		return EXIT_FAILURE;
	}
	// tell GLFW to make the context of our window the main context on the current thread
	glfwMakeContextCurrent(window);

	// pass glad the function to load the OpenGL function pointers
	gladLoadGLLoader((GLADloadproc)glfwGetProcAddress);

	glEnable(GL_DEBUG_OUTPUT);
	glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS);
	glDebugMessageCallback(MessageCallback, 0);
	glDebugMessageControl(GL_DONT_CARE, GL_DONT_CARE, GL_DONT_CARE, 0, nullptr, GL_TRUE);

	glEnable(GL_PROGRAM_POINT_SIZE);

	return 0;
}
