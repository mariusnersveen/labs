// https://learnopengl.com/Getting-started/Textures

// Loading textures
// Read about this from "07_Textures_1"
#include <stb_image.h>

// Handle shaders
#include "shader_s.h"

#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include <iostream>
#include <cmath>

// OpenGL Mathematics
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>


// COORDINATE SYSTEMS
// ------------------
// There are a total of 5 different coordinate systems that are of importance to us:
// * Local space (or Object space)
// * World space
// * View space (or Eye space)
// * Clip space
// * Screen space
// Those are all a different state at which our vertices will be transformed in before finally ending up as fragments.

// THE GLOBAL PICTURE
// To transform coordinates from one space to the next coordinate space we'll use several transformation matrices of which the most important are the model, view and projection matrix. Our vertex coordinates first start in local spacce as local coordinates and are then further processed to world coordinates, view coordinates, clip coordinates and eventually end up as screen coordinates. The following image displays the process and shows what each transformations does: https://learnopengl.com/img/getting-started/coordinate_systems.png
// 1. Local coordinates are the coordinates of your object relative to its local origin; they're the coordinates your object begins in.
// 2. The next step is to transform the local coordinates to world-space coordinates which are coordinates in respect of a larger world. These coordinates are relative to this world origin.
// 3. Next we transform the world coordinates to view-space coordinates in such a way that each coordinate is seen from the camera or viewer's point of view.
// 4. After the coordinates are in ciew space we want to project them to clip coordinates. Clip coordinates are processed to the -1.0 and 1.0 range and determine which vertices will end up on the screen. Projection to clip-space coordinates can add perspective if using perspective projection.
// 5. And lastly we transform the clip coordinates to screen coordinates in a process we call viewport transform that transforms the coordinates from -1.0 and 1.0 to the coordinate range defines by 'glViewport'. The resulting coordinates are then sent to the rasterizer to turn them into fragments.
// You probably got a slight idea what each individual space is used for. The reason we're transforming our vertices into all these different spaces is that some operations make more sense or are easier to use in certain coordinate systems. For example, when modifying your object it makes most sense to do this in local space, while calculating certain operations on the object with respect to the position of other objects makes most sense in world coordinates and so on. If we want, we could define one transformation matrix that goes from local space to clip space all in one go, but that leaves us with less flexibility.
// We'll discuss each coordinate system in more detail below.

// LOCAL SPACE
// Local space is the coordinate space that is local to your object, i.e. where your object begins in. Imagine that you've created your cube in a modeling software package (like Blender). The origin of your cube is probably at (0,0,0) even though your cube may end up at a different location in your final application. Probably all the models you've created all have (0,0,0) as their initial position. All the vertices of your model are therefor in local space: they are all local to your object.
// The vertices of the container we've been using were specified as coordinates between -0.5 and 0.5 with 0.0 as its origin. These are local coordinates.

// WORLD SPACE
// If we would import all our objects directly in the application they would probably all be somewhere positioned inside each other at the world's origin of (0,0,0) which is not what we want. We want to define a position for each object to position them inside a larger world. The coordinates in world space are exactly what they sound like: the coordinates of all your vertices relative to a (game) world. This is the coordinate space where you want your objects transformed to in such a way that they're all scattered around the place (preferably in a realistic fashion). The coordinates of your object are transformed from local to world space; this is accomplished with the model matrix.
// The model matrix is a transformation matrix that translates, scales and/or rotates your object to place it in the world at a location/orientation they belong to. Think of it as transforming a house by scaling it down (it was a bit too large in local space), translating it to a suburbia town and rotating it a bit to the left on the y-axis so that it neatly fits with the neighboring houses. You could think of the matrix in the previous chapter to position the container all over the scene as a sort of model matrix as well; we transformed the local coordinates of the container to some different place in the scene/world.

// VIEW SPACE
// The view space is what people usually refer to as the camera of OpenGL (it is sometimes also know as camera space or eye space). The view space is the result of transforming your world-space coordinates to coordinates that are in front of the user's view. The view space is thus the space as seen from the camera's point of view. This is usually accomplished with a combination of translations and rotations to translate/rotate the scene so that certain items are transformed to the front of the camera. These combined transformations are generally stored inside a view matrix that transforms world coordinates to view space. In the next chapter we'll extensively discuss how to create such a view matrix to simulate a camera.

// CLIP SPACE
// At the end of each vertex shader run, OpenGL expects the coordinates to be within a specific range and any coordinates that falls outside this range is clipped. Coordinates that are clipped are discarded, so the remaining coordinates will end up as fragments visible on your screen. This is also where clip space gets its name from.
// Because specifying all the visible coordinates to be within the range -1.0 and 1.0 itsn't really intuitive, we specify our own coordinate set to work in and covert those back to NDC (normalized device coordinates) as OpenGL expects them.
// To transform vertex coordinates from view to clip-space we define a so called projection matrix that specifies a range of coordinates e.g. -1000 and 1000 in each dimension. The projection matrix then transforms coordinates within this specified range to normalized device coordinates (-1.0, 1.0). All coordinates outside this range will not be mapped between -1.0 and 1.0 and therefore be clipped. With this range we specified in the projection matrix, a coordinate of (1250, 500, 750) would not be visible, since the x coordinate is out of range and thus gets converted to a coordinate higher than 1.0 in NDC and is therefor clipped.
// Note that if only a part of a primitive e.g. a triangle is outside the clipping volume OpenGL will reconstrict the triangle as one or more triangles to fit inside the clipping range.
// The viewing box a projection matrix creates is called a frustum and each coordinate that ends up inside this frustum will end up on the user's screen. The total process to convert coordinates within a specific range to NDC that can easily be mapped to a 2D view-space coordinates is called projection since the projection matrix projects 3D coordinates to the easy-to-map-tp-2D normalized device coordinates.
// Once all the vertices are transformed to clip space a final operation called perspective division is performed where we divide the x, y and z components of the position vectors by the vectors's homogeneous w component; perspective division is what transforms the 4D clip space coordinates to 3D normalized device coordinates. This step is perfomred automatically at the end of the vertex shader step.
// It is after this stage where the resulting coordinates are mapped to screen coordinates (using the settings og 'glViewport') and turned into fragments.
// The projection matrix to transform view coordinates tot clip coordinates usually takes two different forms, where each form defines its own unique frustum. We can either create an orthographic projection matrix or a perspective projection matrix.

// ORTHOGRAPHIC PROJECTION
// An orthographic projection matrix defines a cube-line frustum box that defines the clipping space where each vertex outside the box is clipped. When creating an orthographic projection matrix we specify the width, height and length of the visible frustum. All coordinates inside this frustum will end up within the NDC range after transformed by its matrix and this won't be clipped. The frustum looks a bit like a container: https://learnopengl.com/img/getting-started/orthographic_frustum.png
// The frustum defines the visible coordinates and is specified by a width, a height and a near and far plane. Any coordinates in front of the near plane is clipped and the same applies to coordinates behind the far plane. The orthographic frustum directly maps all coordinates inside the frustum to normalized device coordinates without any special side effects since it won't touch the w components of the transformed vector; if the w component remains equal to 1.0 perspective division won't change the coordinates.
// To create an orthographic projection matrix we make use of GLM's built-in function 'glm::ortho':
/*
    glm::ortho(0.0f, 800.0f, 0.0f, 600.0f, 0.1f, 100.0f);
*/
// The first two parameters specify the left and right coordinate of the frustum and the third and fourth parameter specify the bottom and top part of the frustum. With those 4 points we've defined the size of the near and far planes and the 5th and 6th parameter then defines the distance between the near and far plane. This specific projection matrix transforms all coordinates between these x, y and z range values to normalized device coordinates.
// An orthographic projection matrix directly maps coordinates to the 2D plane that is your screen, but in reality a direct projection produces unrealistic results since the projection doesn't take perspective into account. That is something the perspective matrix fixes for us.

// PERSPECTIVE PROJECTION
// Is described in the render loop

// When using orthographic projection, each of the vertex coordinates are directly mapped to clip space without any fancy perspective division (it doess does perspective division, but the w component is not manipulated (it stays 1) and thus has no effect). Because the orthographic projection doesn't use perspective projection, objects farther do not seem smaller, which produces a weird visual output. For this reason the orthographic projection is mainly used for 2D renderings and for some architectural or engineering application where we'd rather not have vertices distorted by perspective. Applications like Blender that are used for 3D modeling sometimes use orthographic projection for modeling, because it more accurately depicts each object's dimensions. Below you'll see a comparison of both projection methods in Blender: https://learnopengl.com/img/getting-started/perspective_orthographic.png
// You can see that with perspective projection, the vertices farther away appear much smaller, while in orthographic projection each vertex has the same distance to the user.

// PUTTING IT ALL TOGETHER
// We create a transformation matrix for each of the aforementioned steps: model, view and projection matrix. A vertex coordinate is then transformed to clip coordinates as follows:
// (VERTEX)clip = (MATRIX)projection * (MATRIX)view * (MATRIX)model * (VERTEX)local
// Note that the order of matrix multiplication is reversed (remember that we need to read matrix multiplication from right to left). The resulting vertex should then be assigned to 'gl_Position' in the vertex shader and OpenGL will then automatically perform perspective division and clipping.


// RIGHT HANDED SYSTEM
// By convention, OpenGL is a right-handed system. What this basically says is that the positive x-axis is to your right, the positive y-axis is up and the positive z-axis is backwards. Think of your screen being the center of the 3 axes and the positive z-axis goijng through your towards you. The axes are drawn as follows: https://learnopengl.com/img/getting-started/coordinate_systems_right_handed.png
// To understand why it's called right-handed do the following:
// * Stretch your right-arm along the positive y-axis with your hand up top.
// * Let your thumb point to the right.
// * Let your pointing finger point up.
// * Now bend your middle finger downwards 90 degrees.
// If you did things right, your thumb should point towards the positive x-axis, the pointing finger towards the positive y-axis and your middle finger towards the positive z-axis. If you were to do this with your left-arm you would see the z-axis is reversed. This is known as a left-handed system and is commonly used by DirectX. Note that in normalized device coordinates OpenGL actually uses a left-handed system (the projection matrix switched the handedness).



// SHADERS
// -------
// Read about shaders from "04_Shaders_1"



// Read about this from "01_Hello_Window"
void framebuffer_size_callback(GLFWwindow* window, int width, int height);

// Read about this from "01_Hello_Window"
void processInput(GLFWwindow* window);

// Settings
const unsigned int SCR_WIDTH = 800;
const unsigned int SCR_HEIGHT = 600;

// VERTEX SHADER
// Read about this from "02_Hello_Triangle"
// The vertex shader is stored in "vertex.shader"

// FRAGMENT SHADER
// Read about this from "02_Hello_Triangle"
// Read about "uniforms" from "04_Shaders_1"
// The fragment shader is stored in "fragment.shader"

int main()
{
    // GLFW: Initialize and configure
    // ------------------------------
    // Read about this from "01_Hello_Window"
    glfwInit();
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

#ifdef __APPLE__
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
#endif

    // GLFW window creation
    // --------------------
    // Read about this from "01_Hello_Window"
    GLFWwindow* window = glfwCreateWindow(SCR_WIDTH, SCR_HEIGHT, "LearnOpenGL", NULL, NULL);
    if (window == NULL)
    {
        std::cout << "Failed to create GLFW window" << std::endl;
        glfwTerminate();
        return -1;
    }
    glfwMakeContextCurrent(window);
    glfwSetFramebufferSizeCallback(window, framebuffer_size_callback);

    // GLAD: load all OpenGL function pointers
    // ---------------------------------------
    // Read about this from "01_Hello_Window"
    if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
    {
        std::cout << "Failed to initialize GLAD" << std::endl;
        return -1;
    }

    // Prints the max number of vertex attributes supported by the hardware
    // OpenGL guarantees there are always at least 16 4-component vertex attributes available
    int nrAttributes;
    glGetIntegerv(GL_MAX_VERTEX_ATTRIBS, &nrAttributes);
    std::cout << "Maximum nr of vertex attributes supported: " << nrAttributes << std::endl;


    // Build and compile our shader program
    // ------------------------------------
    // The path is the path from the .exe file inside out/build/x64-Debug/bin/
    Shader ourShader("../../../../Not_Labs/LearnOpenGL/Getting_Started/10_Coordinate_Systems_1/vertex.shader", "../../../../Not_Labs/LearnOpenGL/Getting_Started/10_Coordinate_Systems_1/fragment.shader");

    // Set up vertex data (and buffer(s)) and configure vertex attributes
    // ------------------------------------------------------------------
    // Read about this from "02_Hello_Triangle"
    // Read about how one vertex can contain position and color data in "05_Shaders_2"
    // Read about how one vertex can contain position, color and coordinates from "07_Textures_1"

    float vertices[] = {
        // positions          // colors           // texture coords
         0.5f,  0.5f, 0.0f,   1.0f, 0.0f, 0.0f,   1.0f, 1.0f,   // top right
         0.5f, -0.5f, 0.0f,   0.0f, 1.0f, 0.0f,   1.0f, 0.0f,   // bottom right
        -0.5f, -0.5f, 0.0f,   0.0f, 0.0f, 1.0f,   0.0f, 0.0f,   // bottom left
        -0.5f,  0.5f, 0.0f,   1.0f, 1.0f, 0.0f,   0.0f, 1.0f    // top left
    };

    unsigned int indices[] = {
        0, 1, 3, // first triangle
        1, 2, 3  // second triangle
    };

    // Generating array and buffers
    unsigned int VAO, VBO, EBO;
    glGenVertexArrays(1, &VAO);
    glGenBuffers(1, &VBO);
    glGenBuffers(1, &EBO);

    // Bind the Vertex Array Object first, then bind and set vertex buffer(s), and then configure vertex attributes(s).
    glBindVertexArray(VAO);

    glBindBuffer(GL_ARRAY_BUFFER, VBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);


    // LINKING VERTEX ATTRIBUTES
    // Read about this from "02_Hello_Triangle"
    // Read about how one vertex can contain position and color data in "05_Shaders_2"
    // Read about how one vertex can contain position, color and coordinates from "07_Textures_1"

    // Position attribute
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)0);
    glEnableVertexAttribArray(0);
    // Color attribute
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)(3 * sizeof(float)));
    glEnableVertexAttribArray(1);
    // Texture coord attribute
    glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)(6 * sizeof(float)));
    glEnableVertexAttribArray(2);
    // Note that we have to adjust the stride parameter of the previous two vertex attributes to 8 * 'sizeof(float)' as well.


    // You can unbind the VAO afterwards so other VAO calls won't accidentally modify this VAO, but this rarely happens. Modifying other
    // VAOs requires a call to glBindVertexArray anyways so we generally don't unbind VAOs (nor VBOs) when it's not directly necessary.
    //glBindVertexArray(0);

    // Bind the VAO (it was already bound, but just to demonstrate): seeing as we only have a single VAO we can 
    // just bind it beforehand before rendering the respective triangle; this is another approach.
    glBindVertexArray(VAO);


    // Load and create a texture
    // -------------------------
    // Generating a texture
    // Read about this from "07_Textures_1"
    unsigned int texture1, texture2;
    // Texture 1
    // ---------
    glGenTextures(1, &texture1);
    glBindTexture(GL_TEXTURE_2D, texture1);  // all upcoming GL_TEXTURE_2D operations now have effect on this texture object
    // Set the texture wrapping parameters
    // Read about this from "07_Textures_1"
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);  // Set texture wrapping to GL_REPEAT (default wrapping method)
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    // Set texture filtering parameters
    // Read about this from "07_Textures_1"
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    // Load image, create texture and generate mipmaps
    // Read about this from "07_Textures_1"
    int width, height, nrChannels;
    stbi_set_flip_vertically_on_load(true); // tell stb_image.h to flip loaded texture's on the y-axis. More comment from "08_Textures_2"
    unsigned char* data = stbi_load("../../../../Not_Labs/LearnOpenGL/Getting_Started/10_Coordinate_Systems_1/container.jpg", &width, &height, &nrChannels, 0);
    if (data)
    {
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, data);
        glGenerateMipmap(GL_TEXTURE_2D);
    }
    else
    {
        std::cout << "Failed to load texture" << std::endl;
    }
    // Free image memory
    stbi_image_free(data);

    // Texture 2
    // ---------
    // Read about this from "08_Textures_2"
    glGenTextures(1, &texture2);
    glBindTexture(GL_TEXTURE_2D, texture2);  // all upcoming GL_TEXTURE_2D operations now have effect on this texture object
    // Set the texture wrapping parameters
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);  // Set texture wrapping to GL_REPEAT (default wrapping method)
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    // Set texture filtering parameters
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    // Load image, create texture and generate mipmaps
    data = stbi_load("../../../../Not_Labs/LearnOpenGL/Getting_Started/10_Coordinate_Systems_1/awesomeface.png", &width, &height, &nrChannels, 0);
    if (data)
    {
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, data);
        glGenerateMipmap(GL_TEXTURE_2D);
    }
    else
    {
        std::cout << "Failed to load texture" << std::endl;
    }
    // Free image memory
    stbi_image_free(data);

    // Tell OpenGL for each sampler to which texture unit it belongs to (only has to be done once)
    // -------------------------------------------------------------------------------------------
    // Read about this from "08_Textures_2"
    ourShader.use();  // Don't forget to activate/use the shader before settings uniforms!
    glUniform1i(glGetUniformLocation(ourShader.ID, "texture1"), 0);  // Either set it manually
    ourShader.setInt("texture2", 1);// Or set it via the texture/shader class

    // Render loop
    // -----------
    // Read about this from "01_Hello_Window"
    while (!glfwWindowShouldClose(window))
    {
        // Input
        // -----
        processInput(window);

        // Render
        // ------
        // Read about this from "01_Hello_Window"
        glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT);
        
        // Bind textures on corresponding texture units
        // Read about this from "08_Textures_2"
        glActiveTexture(GL_TEXTURE0);  // activate the texture unit first before binding texture
        glBindTexture(GL_TEXTURE_2D, texture1);
        glActiveTexture(GL_TEXTURE1);
        glBindTexture(GL_TEXTURE_2D, texture2);

        // Activate shader
        // Be sure to activate the shader before any calls to 'glUniform'
        ourShader.use();

        // Create transformations
        // Read about this from "09_Transformations"
        glm::mat4 model = glm::mat4(1.0f);  // make sure to initialize matrix to identidy matrix first
        glm::mat4 view = glm::mat4(1.0f);
        glm::mat4 projection = glm::mat4(1.0f);


        // GOING 3D
        // Now that we know how to transform 3D coordinates to 2D coordinates we can start rendering the real 3D objects instead of the lame 2D plane we've been showing so far.
        // To start drawin in 3D we'll first create a model matrix. The model matrix consists of translations, scaling and/or rotation we'd like to apply to transform all object's vertices to the global world space. Let's transform our plane a bit by rotating it on the x-axis so it looks like it's laying on the floor. The model matrix then looks like this:
        model = glm::rotate(model, glm::radians(-55.0f), glm::vec3(1.0f, 0.0f, 0.0f));
        // By multiplying the vertex coordinates with this model matrix we're transforming the vertex coordinates to world coordinates. Our plane that is slightly on the floor thus represents the plane in the global world.
        // Next we need to create a view matrix. We want to move slightly backwards in the scene so the object becomes visible (when in world space we're located at the origin (0,0,0)). To move around the screen, think about the following:
        // * To move a camera backwards, is the same as moving the entire scene forward.
        // This is exactly what a view matrix does, we move the entire scene around inversed to where we want the camera to move. Because we want to move backwards and since OpenGL is a right-handed system we have to move in the positive z-axis. We do this by translating the scene towards the negative z-axis. This gives the impression that we are moving backwards.
        

         // We'll discuss how to move around the scene in more detail in the next chapter. For now the view matrix looks like this:
        view = glm::translate(view, glm::vec3(0.0f, 0.0f, -3.0f));
        // note that we're translating the scene in the reverse direction of where we want to move


        // PERSPECTIVE PROJECTION
        // If you ever were to enjoy the graphics the real life has to offer you'll notice that objects that are farther away appear much smaller. This weird effect is something we call perspective. Perspective is especially noticable when looking down the end of and infinite motorway or railway as seen in the following image: https://learnopengl.com/img/getting-started/perspective.png
        // As you can see, due to perspective the lines seem to coincide at a far enough distance. This is exactly the effect perspective projection tries to mimic and it does so using a perspective projection matrix. The projection matrix maps a given frustum range to clip space, but also manipulates the w value of each vertex coordinate in such a way that the further away a vertex coordinate is from the viewer, the higher this w component becomes. Once the coordinates are transformed to clip space they are in the range -w to w (anything outside this range is clipped). OpenGL requires that the visible coordinates fall between the range -1.0 and 1.0 as the final vertex shader output, thus once coordinates are in clip space, perspective division is applied to the clip space coordinates:
        //       ( x/w )
        // out = ( y/w )
        //       ( z/w )
        // Each component of the vertex coordinate is divided by its w component giving smaller vertex coordinates the further away a vertex is from the viewer. This is another reason why the w component is important, since it helps us with perspective projection. The resulting coordinates are then in normalized device space. If you're interested to figure out how the orthographic and perspective projection matrices are actually calculated (and aren't too scared of the mathematics) I can recoment this excellent article by Songho: http://www.songho.ca/opengl/gl_projectionmatrix.html
        // A perspective projection matrix can be created in GLM as follows:
        projection = glm::perspective(glm::radians(45.0f), (float)SCR_WIDTH / (float)SCR_HEIGHT, 0.1f, 100.0f);
        // What 'glm::perspective' does is again create a large frustum that defines the visible space, anything outside the frustum will not end up in the clip space volume and will thus become clipped. A perspective frustum can be visualized as a non-uniformly shaped box from where each coordinate inside this box will be mapped to a point in clip space. An image of a persoective frustum is seen below: https://learnopengl.com/img/getting-started/perspective_frustum.png
        // Its first parameter defines the fov value, that stand for field of view and sets up how large the viewspace is. For a realistic view it is usually set to 45 degrees, but for more doom-style results you could set it to a higher value. The second parameter sets the aspect ratio which is calculated by dividing the viewport's width by its height. The third and fourth parameter set the near and far plane of the frustum. We usually set the nera distance to 0.1 and the far distance to 100.0. All the vertices between the near and far plane and inside the frustum will be rendered.
        // NOTE: Whenever the nera value of your perspective matrix is set too high (like 10.0), OpenGL will clip all coordinates close to the camera (between 0.0 and 10.0), which can give a visual result you maybe have seen before in videogames where you could see through certain objects when moving uncomfortably close to them.


        // Retrieve the matrix uniform locations
        unsigned int modelLoc = glGetUniformLocation(ourShader.ID, "model");
        unsigned int viewLoc = glGetUniformLocation(ourShader.ID, "view");
        // We should also send the matrices to the shader (this is usually done each frame since transformation matrices tend to change a lot):
        // Pass them to the shaders (3 different ways)
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glUniformMatrix4fv(viewLoc, 1, GL_FALSE, &view[0][0]);
        // Note: currently we set the projection matrix each frame, but since the projection matrix rarely changes it's often best practice to set it outside the main loop only once.
        ourShader.setMat4("projection", projection);

        // Now that our vertex coordinates are transformed via the model, view and projection matrix the final object should be:
        // * Tilted backwards to the floor.
        // * A bit farther away from us.
        // * Be displayed with perspective (it should get smaller, the further its vertices are).

        //glBindVertexArray(VAO);  // This is not needed since we only have 1 vertex array and we bounded it before the render loop
        glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);

        // GLFW: Swap buffers and poll IO events (key pressed/released, mouse moved etc.)
        // ------------------------------------------------------------------------------
        // Read about this from "01_Hello_Window"
        glfwSwapBuffers(window);
        glfwPollEvents();
    }
    // Optional: de-allocate all resources once they've outlived their purpose:
    // ------------------------------------------------------------------------
    glDeleteVertexArrays(1, &VAO);
    glDeleteBuffers(1, &VBO);
    glDeleteBuffers(1, &EBO);

    // GLFW: Terminate, clearing all previously allocated GLFW resources.
    // ------------------------------------------------------------------
    // Read about this from "01_Hello_Window"
    glfwTerminate();
    return 0;
}

// GLFW: whenever the window size changed (by OS or user resize) this callback function executes
// ---------------------------------------------------------------------------------------------
void framebuffer_size_callback(GLFWwindow* window, int width, int height)
{
    // make sure the viewport matches the new window dimensions; note that width and 
    // height will be significantly larger than specified on retina displays.
    glViewport(0, 0, width, height);
}

// Process all input: query GLFW whether relevant keys are pressed/released this frame and react accordingly
// ---------------------------------------------------------------------------------------------------------
void processInput(GLFWwindow* window)
{
    if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
        glfwSetWindowShouldClose(window, true);
}