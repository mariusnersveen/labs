// https://learnopengl.com/Getting-started/Shaders

// SHADERS
// -------
// Shaders always begin with a version declaration, followed by a list of input and output variables, unforms and its 'main' function. Each shader's entry point is at its 'main' function function where we process any input variables and output the results in its output variables. Don't worry if you don't know what uniforms are, we'll get to those shortly.
// A shader typically has the following structure:
/*
    #version version_number
    in type in_variable_name;
    in type in_variable_name;

    out type out_variable_name;

    uniform type uniform_name;

    void main()
    {
      // process input(s) and do some weird graphics stuff
      ...
      // output processed stuff to output variable
      out_variable_name = weird_stuff_we_processed;
    }
*/
// When we're talking specifically about the vertex shader each input variable is also known as a "vertex attribute". There is a maximum number of vertex attributes we're allowed to declare limited by the hardware. OpenGL guarantees there are always at least 16 4-component vertex attributes available, but some hardware may allow for more which you can retrieve by querying 'GL_MAX_VERTEX_ATTRIBS':
// (This wont work here because GLFW and GLAD is not initialized)
/*
int nrAttributes;
glGetIntegerv(GL_MAX_VERTEX_ATTRIBS, &nrAttributes);
std::cout << "Maximum nr of vertex attributes supported: " << nrAttributes << std::endl;
*/
// This often returns the minimum of 16 which should be more than enough for most purposes

// TYPES
// -----
// GLSL has, like any other programming language, data types for specifying what kind of variable we want to work with. GLSL has most of the default basic types we know from languages like C: 'int', 'float', 'double', 'uint' and 'bool'. GLSL also features two container types that we'll be using a lot, namely "vectors" and "matrices".

// Vectors
// -------
// A vector in GLSL is a 2, 3 or 4 component container for any of the basic types just mentioned. They can take the following form (n represents the number of components):
// * 'vecn': the default vector of n floats
// * 'bvecn': a vector of n booleans
// * 'ivecn': a vector of n integers
// * 'uvecn': a vector of n unsigned integers
// * 'dvecn': a vector of n double components
// Most of the time we will be using a basic 'vecn' since floats are sufficient for most of our purposes.

// Components of a vector can be accessed via vec.x where x is the first component of the vector. You can use .x, .y, .z and .w to access their first, second, third and fourth component respectively. GLSL also allows you to use rgba for colors or stpq for texture coordinates, accessing the same components.

// The vector datatype allows for some interesting and flexible component selection called "swizzling". Swizzling allows us to use syntax like this:
/*
    vec2 someVec;
    vec4 differentVec = someVec.xyxx;
    vec3 anotherVec = differentVec.zyw;
    vec4 otherVec = someVec.xxxx + anotherVec.yxzy;
*/

// You can use any combination of up to 4 letters to create a new vector (of the same type) as long as the original vector has those components; it is not allowed toaccess the .z component of a vec2 for example. We can also pass vectors as arguments to different vector constructor calls, reducing the number of arguments required:
/*
    vec2 vect = vec2(0.5, 0.7);
    vec4 result = vec4(vect, 0.0, 0.0);
    vec4 otherResult = vec4(result.xyz, 1.0);
*/

// Vectors are thus a flexible datatype that we can use for all kinds of input and output.

// INS AND OUTS
// ------------
// Shaders are nice little programs on their own, but they are part of a whole and for that reason we want to have inputs and outputs on the individual shaders so that we can move stuff around. GLSL defines the 'in' and 'out' keyword specifically for that purpose. Each shader can specify inputs and outputs using those keywords and wherever an output variable matches with an input variable of the next shader stage they're passed along. The vertex and fragment shader differ a bit though.
// The vertex shader SHOULD receive some form of input otherwise it would be pretty ineffective. The vertex shader differs in its input, in that it receives its input straight from the vertex data. To define how to vertex data is organized we specify the input variables with 'location' metadata so we can configure the vertex attributes on the CPU. We've seen this in the previous code as "layout (location = 0)". The vertex shader thus requires and extra layout specification for its input so we can link it with the vertex data.

// The other exception is that the fragment shader required a 'vec4' color output variable, since the fragment shaders needs to generate a final output color. If you fail to specify an output color in your fragment shader, the color buffer output for those fragments will be undefined (which usually means OpenGL will render them either black or white).
// So if we want to send data from one shader to the other we'd have to declare an output in the sending shader and a similar input in the receiving shader. When the types and the names are equal on both sidens OpenGL will link those variables together and then it is possible to send data between shaders (this is done when linking a program object). To show you how this works in practise we're going to alter the shaders from the previous code to let the vertex shader decide the color for the fragment shader.

// VERTEX SHADER
/*
    #version 330 core
    layout (location = 0) in vec3 aPos; // the position variable has attribute position 0

    out vec4 vertexColor; // specify a color output to the fragment shader

    void main()
    {
        gl_Position = vec4(aPos, 1.0); // see how we directly give a vec3 to vec4's constructor
        vertexColor = vec4(0.5, 0.0, 0.0, 1.0); // set the output variable to a dark-red color
    }
*/
// FRAGMENT SHADER
/*
    #version 330 core
    out vec4 FragColor;

    in vec4 vertexColor; // the input variable from the vertex shader (same name and same type)

    void main()
    {
        FragColor = vertexColor;
    }
*/
// We can see we declared a 'vertexColor' variable as a vec4 output that we set in the vertex shader and we declare a similar 'vertexColor' input in the fragment shader. Since they both have the same type and name, the 'vertexColor' in the fragment shader in linked to the 'vertexColor' in the vertex shader. Because we set the color to a dark-red color in the vertex shader, the resulting fragments should be dark-red as well.
// We just managed to send a value from the vertex shader to the fragment shader. Let's spice it up a bit and see if we can send a color from our application to the fragment shader using uniforms.


#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include <iostream>
#include <cmath>


// Read about this from "01_Hello_Window"
void framebuffer_size_callback(GLFWwindow* window, int width, int height);

// Read about this from "01_Hello_Window"
void processInput(GLFWwindow* window);

// Settings
const unsigned int SCR_WIDTH = 800;
const unsigned int SCR_HEIGHT = 600;

// VERTEX SHADER
// Read about this from "02_Hello_Triangle"
const char* vertexShaderSource = 
"#version 330 core\n"
"layout (location = 0) in vec3 aPos;\n"
"void main()\n"
"{\n"
"   gl_Position = vec4(aPos.x, aPos.y, aPos.z, 1.0);\n"
"}\0";

// FRAGMENT SHADER
// Read about this from "02_Hello_Triangle"
// 
// UNIFORMS
// "Uniforms" are another way to pass data from our application on the CPU to the shaders on the GPU. Uniforms are however slightly different compared to vertex attributes. First of all, uniforms are gloabl. Global, meaning that a uniform variable is unique per shader program object, and can be accessed from any shader at any stage in the shader program. Second, whatever you set the uniform value to, uniforms will keep their value until they're either reset or updated.
// To declare a uniform in GLSL we simply add the 'uniform' keyword to a shader with a type and a name. From that point on we can use the newly declared uniform in the shader. Let's see if this time we can set the color of the triangle via a uniform:
const char* fragmentShaderSource = 
"#version 330 core\n"
"out vec4 FragColor;\n"
"uniform vec4 ourColor;\n"
"void main()\n"
"{\n"
"   FragColor = ourColor;\n"
"}\n\0";
// We declared a uniform vec4 'ourColor' in the fragment shader and set the fragment's output color to the content of this uniform value. Since uniforms are global variables, we can define them in any shader stage we'd like so no need to go through the vertex shader again to get something to the fragment shader. We're not using this uniform in the vertex shader so there's no need to define it there.
// NOTE: If you declare a uniform that isn't used anywhere in your GLSL code the compiler will silently remove the variable from the compiled version which is the cause for several frustrating errors; keep this in mind!
// The uniform is currently empty; we haven't added any data to the uniform yet so let's try that:
// To add data to the uniform we first need to find the index/location of the uniform attribute in our shader. Once we have the index/location of the uniform, we can update its values. In the Render loop under "Update shader uniform" we're doing this and spicing it up by gradually changing color over time.


int main()
{
    // GLFW: Initialize and configure
    // ------------------------------
    // Read about this from "01_Hello_Window"
    glfwInit();
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

#ifdef __APPLE__
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
#endif

    // GLFW window creation
    // --------------------
    // Read about this from "01_Hello_Window"
    GLFWwindow* window = glfwCreateWindow(SCR_WIDTH, SCR_HEIGHT, "LearnOpenGL", NULL, NULL);
    if (window == NULL)
    {
        std::cout << "Failed to create GLFW window" << std::endl;
        glfwTerminate();
        return -1;
    }
    glfwMakeContextCurrent(window);
    glfwSetFramebufferSizeCallback(window, framebuffer_size_callback);

    // GLAD: load all OpenGL function pointers
    // ---------------------------------------
    // Read about this from "01_Hello_Window"
    if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
    {
        std::cout << "Failed to initialize GLAD" << std::endl;
        return -1;
    }

    // Prints the max number of vertex attributes supported by the hardware
    // OpenGL guarantees there are always at least 16 4-component vertex attributes available
    int nrAttributes;
    glGetIntegerv(GL_MAX_VERTEX_ATTRIBS, &nrAttributes);
    std::cout << "Maximum nr of vertex attributes supported: " << nrAttributes << std::endl;
   


    // Build and compile our shader program
    // ------------------------------------
    // VERTEX SHADER
    // Read about this from "02_Hello_Triangle"
    unsigned int vertexShader = glCreateShader(GL_VERTEX_SHADER);
    glShaderSource(vertexShader, 1, &vertexShaderSource, NULL);
    glCompileShader(vertexShader);
    // CHECK FOR SHADER COMPILE ERRORS
    // Read about this from "02_Hello_Triangle"
    int success;
    char infoLog[512];
    glGetShaderiv(vertexShader, GL_COMPILE_STATUS, &success);
    if (!success)
    {
        glGetShaderInfoLog(vertexShader, 512, NULL, infoLog);
        std::cout << "ERROR::SHADER::VERTEX::COMPILATION_FAILED\n" << infoLog << std::endl;
    }
    // FRAGMENT SHADER
    // Read about this from "02_Hello_Triangle"
    unsigned int fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(fragmentShader, 1, &fragmentShaderSource, NULL);
    glCompileShader(fragmentShader);
    // CHECK FOR SHADER COMPILE ERRORS
    glGetShaderiv(fragmentShader, GL_COMPILE_STATUS, &success);
    if (!success)
    {
        glGetShaderInfoLog(fragmentShader, 512, NULL, infoLog);
        std::cout << "ERROR::SHADER::FRAGMENT::COMPILATION_FAILED\n" << infoLog << std::endl;
    }
    // LINK SHADERS
    // Read about this from "02_Hello_Triangle"
    unsigned int shaderProgram = glCreateProgram();
    glAttachShader(shaderProgram, vertexShader);
    glAttachShader(shaderProgram, fragmentShader);
    glLinkProgram(shaderProgram);
    // CHECK FOR LINKING ERRORS
    glGetProgramiv(shaderProgram, GL_LINK_STATUS, &success);
    if (!success) {
        glGetProgramInfoLog(shaderProgram, 512, NULL, infoLog);
        std::cout << "ERROR::SHADER::PROGRAM::LINKING_FAILED\n" << infoLog << std::endl;
    }
    // Delete shader objects after linking them
    glDeleteShader(vertexShader);
    glDeleteShader(fragmentShader);

    // Set up vertex data (and buffer(s)) and configure vertex attributes
    // ------------------------------------------------------------------
    // Read about this from "02_Hello_Triangle"
    float vertices[] = {
         0.5f, -0.5f, 0.0f,  // bottom right
        -0.5f, -0.5f, 0.0f,  // bottom left
         0.0f,  0.5f, 0.0f   // top 
    };

    unsigned int VAO, VBO;
    glGenVertexArrays(1, &VAO);
    glGenBuffers(1, &VBO);
    // Bind the Vertex Array Object first, then bind and set vertex buffer(s), and then configure vertex attributes(s).
    glBindVertexArray(VAO);

    glBindBuffer(GL_ARRAY_BUFFER, VBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);


    // LINKING VERTEX ATTRIBUTES
    // Read about this from "02_Hello_Triangle"
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void*)0);
    glEnableVertexAttribArray(0);

    // You can unbind the VAO afterwards so other VAO calls won't accidentally modify this VAO, but this rarely happens. Modifying other
    // VAOs requires a call to glBindVertexArray anyways so we generally don't unbind VAOs (nor VBOs) when it's not directly necessary.
    //glBindVertexArray(0);


    // Bind the VAO (it was already bound, but just to demonstrate): seeing as we only have a single VAO we can 
    // just bind it beforehand before rendering the respective triangle; this is another approach.
    glBindVertexArray(VAO);


    // Render loop
    // -----------
    // Read about this from "01_Hello_Window"
    while (!glfwWindowShouldClose(window))
    {
        // Input
        // -----
        processInput(window);

        // Render
        // ------
        // Read about this from "01_Hello_Window"
        glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT);

        // Be sure to activate the shader before any calls to 'glUniform'
        glUseProgram(shaderProgram);

        // Update shader uniform
        float timeValue = glfwGetTime();
        float greenvalue = sin(timeValue) / 2.0f + 0.5f;
        int vertexColorLocation = glGetUniformLocation(shaderProgram, "ourColor");
        glUniform4f(vertexColorLocation, 0.0f, greenvalue, 0.0f, 1.0f);
        // First, we retrieve the running time in seconds via 'glfwGetTime()'. Then we vary the color in the range of 0.0 - 1.0 by using the 'sin' function and store the result in 'greenValue'.
        // Then we query for the location of the 'ourColor' uniform using 'glGetUniformLocation'. We supply the shader program and the name of the unform (that we want to retrieve the location from) to the query function. If 'glGetUniformLocation' returns -1, it could not find the location. Lastly we can set the uniform value using the 'glUniform4f' function. Note that finding the uniform location does not require you to use the shader profram first, but updating a uniform DOES require you to first use the program (by calling 'glUseProgram'), because it sets the uniform on the currently active shader program.

        // Because OpenGL is in its core a C library it does not have native support for function overloading, so whenever a function can be called with different types OpenGL defines new functions for each type required; 'glUniform' is a perfect example of this. The function requires a specific postfix for the type of the uniform you want to set. A few of the possible postfixes are:
        // * f: the function expects a 'float' as its value.
        // * i: the function expects an 'int' as its value.
        // * ui: the function expects an 'unsigned int' as its value.
        // * 3f: the function expects 3 'floats' as its value.
        // * fv: the function expects a 'float' vector/array as its value.
        // Whenever you want to configure an option of OpenGL simply pick the overloaded function that corresponds with your type. In our case we want to set 4 floats of the uniform individually so we pass our data via 'glUniform4f' (note that we also could've used the 'fv' version).

        // NOTE: If we wanted the triangle to have a single solid/static color we would have just needed to set the color using 'glUniform' before the render loop



        // Render the triangle
        //glBindVertexArray(VAO);  // This is not needed since we only have 1 vertex array and we bounded it before the render loop
        glDrawArrays(GL_TRIANGLES, 0, 3);

        // GLFW: Swap buffers and poll IO events (key pressed/released, mouse moved etc.)
        // ------------------------------------------------------------------------------
        // Read about this from "01_Hello_Window"
        glfwSwapBuffers(window);
        glfwPollEvents();
    }
    // Optional: de-allocate all resources once they've outlived their purpose:
    // ------------------------------------------------------------------------
    glDeleteVertexArrays(1, &VAO);
    glDeleteBuffers(1, &VBO);
    glDeleteProgram(shaderProgram);

    // GLFW: Terminate, clearing all previously allocated GLFW resources.
    // ------------------------------------------------------------------
    // Read about this from "01_Hello_Window"
    glfwTerminate();
    return 0;
}

// GLFW: whenever the window size changed (by OS or user resize) this callback function executes
// ---------------------------------------------------------------------------------------------
void framebuffer_size_callback(GLFWwindow* window, int width, int height)
{
    // make sure the viewport matches the new window dimensions; note that width and 
    // height will be significantly larger than specified on retina displays.
    glViewport(0, 0, width, height);
}

// Process all input: query GLFW whether relevant keys are pressed/released this frame and react accordingly
// ---------------------------------------------------------------------------------------------------------
void processInput(GLFWwindow* window)
{
    if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
        glfwSetWindowShouldClose(window, true);
}