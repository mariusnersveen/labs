// https://learnopengl.com/Getting-started/Textures

// SHADERS
// -------
// Read about shaders from "04_Shaders_1"


// Texture units
// You probably wondered why the 'sampler2D' variable is a uniform if we didn't even assign it some value with 'glUniform'. Using 'glUniform1i' we can actually assign a location value to the texture sampler so we can set multiple textures at once in a fragment shader. This location of a texture is more commonly known as a texture unit. The default texture unit for a texture is 0 which is the default active texture unit so we didn't need to assign a location in the previous section; note that not all graphics drivers assign a default texture unit so the previous section may not have rendered for you.


// Loading textures
// Read about this from "07_Textures_1"
#include <stb_image.h>

// Handle shaders
#include "shader_s.h"

#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include <iostream>
#include <cmath>

// Read about this from "01_Hello_Window"
void framebuffer_size_callback(GLFWwindow* window, int width, int height);

// Read about this from "01_Hello_Window"
void processInput(GLFWwindow* window);

// Settings
const unsigned int SCR_WIDTH = 800;
const unsigned int SCR_HEIGHT = 600;

// VERTEX SHADER
// Read about this from "02_Hello_Triangle"
// The vertex shader is stored in "shader.vs"

// FRAGMENT SHADER
// Read about this from "02_Hello_Triangle"
// Read about "uniforms" from "04_Shaders_1"
// The fragment shader is stored in "shader.fs"

int main()
{
    // GLFW: Initialize and configure
    // ------------------------------
    // Read about this from "01_Hello_Window"
    glfwInit();
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

#ifdef __APPLE__
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
#endif

    // GLFW window creation
    // --------------------
    // Read about this from "01_Hello_Window"
    GLFWwindow* window = glfwCreateWindow(SCR_WIDTH, SCR_HEIGHT, "LearnOpenGL", NULL, NULL);
    if (window == NULL)
    {
        std::cout << "Failed to create GLFW window" << std::endl;
        glfwTerminate();
        return -1;
    }
    glfwMakeContextCurrent(window);
    glfwSetFramebufferSizeCallback(window, framebuffer_size_callback);

    // GLAD: load all OpenGL function pointers
    // ---------------------------------------
    // Read about this from "01_Hello_Window"
    if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
    {
        std::cout << "Failed to initialize GLAD" << std::endl;
        return -1;
    }

    // Prints the max number of vertex attributes supported by the hardware
    // OpenGL guarantees there are always at least 16 4-component vertex attributes available
    int nrAttributes;
    glGetIntegerv(GL_MAX_VERTEX_ATTRIBS, &nrAttributes);
    std::cout << "Maximum nr of vertex attributes supported: " << nrAttributes << std::endl;


    // Build and compile our shader program
    // ------------------------------------
    // The path is the path from the .exe file inside out/build/x64-Debug/bin/
    Shader ourShader("../../../../Not_Labs/LearnOpenGL/Getting_Started/08_Textures_2/shader.vs", "../../../../Not_Labs/LearnOpenGL/Getting_Started/08_Textures_2/shader.fs");

    // Set up vertex data (and buffer(s)) and configure vertex attributes
    // ------------------------------------------------------------------
    // Read about this from "02_Hello_Triangle"
    // Read about how one vertex can contain position and color data in "05_Shaders_2"
    // Read about how one vertex can contain position, color and coordinates from "07_Textures_1"

    float vertices[] = {
        // positions          // colors           // texture coords
         0.5f,  0.5f, 0.0f,   1.0f, 0.0f, 0.0f,   1.0f, 1.0f,   // top right
         0.5f, -0.5f, 0.0f,   0.0f, 1.0f, 0.0f,   1.0f, 0.0f,   // bottom right
        -0.5f, -0.5f, 0.0f,   0.0f, 0.0f, 1.0f,   0.0f, 0.0f,   // bottom left
        -0.5f,  0.5f, 0.0f,   1.0f, 1.0f, 0.0f,   0.0f, 1.0f    // top left
    };

    unsigned int indices[] = {
        0, 1, 3, // first triangle
        1, 2, 3  // second triangle
    };

    // Generating array and buffers
    unsigned int VAO, VBO, EBO;
    glGenVertexArrays(1, &VAO);
    glGenBuffers(1, &VBO);
    glGenBuffers(1, &EBO);

    // Bind the Vertex Array Object first, then bind and set vertex buffer(s), and then configure vertex attributes(s).
    glBindVertexArray(VAO);

    glBindBuffer(GL_ARRAY_BUFFER, VBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);


    // LINKING VERTEX ATTRIBUTES
    // Read about this from "02_Hello_Triangle"
    // Read about how one vertex can contain position and color data in "05_Shaders_2"
    // Read about how one vertex can contain position, color and coordinates from "07_Textures_1"

    // Position attribute
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)0);
    glEnableVertexAttribArray(0);
    // Color attribute
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)(3 * sizeof(float)));
    glEnableVertexAttribArray(1);
    // Texture coord attribute
    glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)(6 * sizeof(float)));
    glEnableVertexAttribArray(2);
    // Note that we have to adjust the stride parameter of the previous two vertex attributes to 8 * 'sizeof(float)' as well.


    // You can unbind the VAO afterwards so other VAO calls won't accidentally modify this VAO, but this rarely happens. Modifying other
    // VAOs requires a call to glBindVertexArray anyways so we generally don't unbind VAOs (nor VBOs) when it's not directly necessary.
    //glBindVertexArray(0);

    // Bind the VAO (it was already bound, but just to demonstrate): seeing as we only have a single VAO we can 
    // just bind it beforehand before rendering the respective triangle; this is another approach.
    glBindVertexArray(VAO);


    // Load and create a texture
    // -------------------------
    // Generating a texture
    // Read about this from "07_Textures_1"
    unsigned int texture1, texture2;
    // Texture 1
    // ---------
    glGenTextures(1, &texture1);
    glBindTexture(GL_TEXTURE_2D, texture1);  // all upcoming GL_TEXTURE_2D operations now have effect on this texture object
    // Set the texture wrapping parameters
    // Read about this from "07_Textures_1"
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);  // Set texture wrapping to GL_REPEAT (default wrapping method)
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    // Set texture filtering parameters
    // Read about this from "07_Textures_1"
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    // Load image, create texture and generate mipmaps
    // Read about this from "07_Textures_1"
    int width, height, nrChannels;
    stbi_set_flip_vertically_on_load(true); // tell stb_image.h to flip loaded texture's on the y-axis.
    // Without 'stbi_set_flip_vertically_on_load(true)' OpenGL will expect the 0.0 coordinate on the y-axis to be on the bottom side of the image, but images usually have 0.0 at the top of the y-axis. Luckily for us, stb_image.h can flip the y-axis during image loading by adding this statement before loading any image.
    unsigned char* data = stbi_load("../../../../Not_Labs/LearnOpenGL/Getting_Started/08_Textures_2/container.jpg", &width, &height, &nrChannels, 0);
    if (data)
    {
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, data);
        glGenerateMipmap(GL_TEXTURE_2D);
    }
    else
    {
        std::cout << "Failed to load texture" << std::endl;
    }
    // Free image memory
    stbi_image_free(data);

    // Texture 2
    // ---------
    // We now want to load and create another texture; you should be familiar with the steps now. Make sure to create another texture object (done above as 'texture2'), load the image and generate the final texture using 'glTexImage2D'. For the second texture we'll use an image of your facial expression while learning OpenGL(https://learnopengl.com/img/textures/awesomeface.png):
    glGenTextures(1, &texture2);
    glBindTexture(GL_TEXTURE_2D, texture2);  // all upcoming GL_TEXTURE_2D operations now have effect on this texture object
    // Set the texture wrapping parameters
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);  // Set texture wrapping to GL_REPEAT (default wrapping method)
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    // Set texture filtering parameters
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    // Load image, create texture and generate mipmaps
    data = stbi_load("../../../../Not_Labs/LearnOpenGL/Getting_Started/08_Textures_2/awesomeface.png", &width, &height, &nrChannels, 0);
    if (data)
    {
        // Note that we load a .png that includes an alpha (transparency) channel. This means we now need to specify that the image data contains an alpha channel as well by using 'GL_RGBA'; otherwise OpenGL will incorrectly interpret the image data.
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, data);
        glGenerateMipmap(GL_TEXTURE_2D);
    }
    else
    {
        std::cout << "Failed to load texture" << std::endl;
    }
    // Free image memory
    stbi_image_free(data);


    // Tell OpenGL for each sampler to which texture unit it belongs to (only has to be done once)
    // -------------------------------------------------------------------------------------------
    // We also have to tell OpenGL which texture unit each shader sampler belongs to by setting each sampler using 'glUniform1i'. We only have to set this once, so we can do this before we enter the render loop:
    ourShader.use();  // Don't forget to activate/use the shader before settings uniforms!
    glUniform1i(glGetUniformLocation(ourShader.ID, "texture1"), 0);  // Either set it manually
    ourShader.setInt("texture2", 1);// Or set it via the texture/shader class
    // By settings the samplers via 'glUniform1i' we make sure each uniform sampler corresponds to the proper texture unit.


    // Render loop
    // -----------
    // Read about this from "01_Hello_Window"
    while (!glfwWindowShouldClose(window))
    {
        // Input
        // -----
        processInput(window);

        // Render
        // ------
        // Read about this from "01_Hello_Window"
        glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT);
        
        // Bind textures on corresponding texture units
        // The main purpose of the texture units is to allow us to use more than 1 texture in our shaders. By assigning texture units to the sampler, we can bind to multiple textures at once as long as we activate the corresponding texture unit first. Just like 'glBindTexture' we can activate texture units using 'glActivateTexture' passing in the texture unit we'd like to use:
        glActiveTexture(GL_TEXTURE0);  // activate the texture unit first before binding texture
        glBindTexture(GL_TEXTURE_2D, texture1);
        // After activating a texture unit, a subsequent 'glBindTexture' call will bind that texture to the currently active texture unit. Texture unit 'GL_TEXTURE0' is always by default activated, so we didn't have to activate any texture units in the previous example when using 'glBindTexture'.
        // NOTE: OpenGL should have at least a minimum of 16 texture units for you to use which you can activate using 'GL_TEXTURE0' to 'GL_TEXTURE15'. They are defined in order so we could also get 'GL_TEXTURE8' via 'GL_TEXTURE0' + 8 for example, which is useful when we'd have to loop over several texture units.
        // To use the second texture (and the first texture) we'd have to change the rendering procedure a bit by binding both textures to the corresponding texture unit.
        glActiveTexture(GL_TEXTURE1);
        glBindTexture(GL_TEXTURE_2D, texture2);

        // Render container
        // Be sure to activate the shader before any calls to 'glUniform'
        ourShader.use();
        //glBindVertexArray(VAO);  // This is not needed since we only have 1 vertex array and we bounded it before the render loop
        glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);

        // GLFW: Swap buffers and poll IO events (key pressed/released, mouse moved etc.)
        // ------------------------------------------------------------------------------
        // Read about this from "01_Hello_Window"
        glfwSwapBuffers(window);
        glfwPollEvents();
    }
    // Optional: de-allocate all resources once they've outlived their purpose:
    // ------------------------------------------------------------------------
    glDeleteVertexArrays(1, &VAO);
    glDeleteBuffers(1, &VBO);
    glDeleteBuffers(1, &EBO);

    // GLFW: Terminate, clearing all previously allocated GLFW resources.
    // ------------------------------------------------------------------
    // Read about this from "01_Hello_Window"
    glfwTerminate();
    return 0;
}

// GLFW: whenever the window size changed (by OS or user resize) this callback function executes
// ---------------------------------------------------------------------------------------------
void framebuffer_size_callback(GLFWwindow* window, int width, int height)
{
    // make sure the viewport matches the new window dimensions; note that width and 
    // height will be significantly larger than specified on retina displays.
    glViewport(0, 0, width, height);
}

// Process all input: query GLFW whether relevant keys are pressed/released this frame and react accordingly
// ---------------------------------------------------------------------------------------------------------
void processInput(GLFWwindow* window)
{
    if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
        glfwSetWindowShouldClose(window, true);
}