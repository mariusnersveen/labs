// We need to alter the vertex shader to accept the texture coordinates as a vertex attribute and then forward the coordinates to the fragment shader:
#version 330 core
layout (location = 0) in vec3 aPos;       // The position variable has attribute position 0
layout (location = 1) in vec3 aColor;     // The color variable has attribute position 1
layout (location = 2) in vec2 aTexCoord;  // The coordinate variable has attribute position 1

out vec3 ourColor;  // Input a color to the fragment shader
out vec2 TexCoord;  // Input a coordinate to the fragment shader

void main()
{
    gl_Position = vec4(aPos, 1.0);
    ourColor = aColor;  // Set 'ourColor' to the input color we got from the vertex data
    TexCoord = aTexCoord;
}
// The fragment shader should now accept the 'TexCoord' output variable as an input variable.