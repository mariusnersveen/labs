add_executable(          # This command will instruct CMake to generate a executable
  Shaders_3              # This parameter is the name of the executable
  main.cpp               # This parameter (and possibly subsequent) are the source code files
                         # participating in the compilation.
  "shader_s.h")

target_link_libraries(Shaders_3 PRIVATE glad)
target_link_libraries(Shaders_3 PRIVATE glfw)