// https://learnopengl.com/Getting-started/Hello-Triangle
#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include <iostream>

// Read about this from "01_Hello_Window"
void framebuffer_size_callback(GLFWwindow* window, int width, int height);

// Read about this from "01_Hello_Window"
void processInput(GLFWwindow* window);

// Settings
const unsigned int SCR_WIDTH = 800;
const unsigned int SCR_HEIGHT = 600;

// The vertex shader is one of the shaders that are programmable by people like us. Modern OpenGL requires that we at least set up a vertex and fragment shader if we want to do some rendering so we will briefly introduce shaders and configure two very simple shaders for drawing our first triangle.
// We need to do is write the vertex shader in the shader language GLSL (OpenGL Shading Language) and then compile this shader so we can use it in our application
// We write the source code for the vertex shader and store in in a 'const char string' at the top of the file for now:
const char* vertexShaderSource = "#version 330 core\n"
"layout (location = 0) in vec3 aPos;\n"
"void main()\n"
"{\n"
"   gl_Position = vec4(aPos.x, aPos.y, aPos.z, 1.0);\n"
"}\0";
// As you can see GLSL looks similar to C. Each shader begins with a declaration of its version. Since OpenGL 3.3 and higher the version numbers of GLSL match the version of OpenGL (GLSL version 420 corresponds to OpenGL 4.2 for example). We also explicitly mention we're using core profile functionality.
// Next we declare all the input vertex attributes in the vertex shader with the 'in' keyword. Right now we only care about position data so we only need a single vertex attribute. GLSL has a vector datatype that contains 1 to 4 floats based on its postfix digit. Since each vertex has a 3D coordinate we create a vec3 input variable with the name aPos. We also specifically set the location of the input variable via 'layout (location = 0)' and we'll later see that why we're going to need that location.
// To set the output of the vertex shader we have to assigne the position data to the predefined 'gl_Position' variable which is a vec4 behind the scenes. At the end of the 'main' function, whatever we set 'gl_Position' to will be used as the output of the vertex shader. Since out input is a vector of size 3, we have to cast this to a vector of size 4. We can do this by inserting the vec3 values inside the constructor of vec4 and set its 'w' component to 1.0f.
// The current vertex shader is probably the most simple vertex shader we can imagine because we did no processing whatsoever on the input data and simply forwarded it to the shader's output. In real applications the input data is usually not already in normalized device coordinates so we first have to transform the input data to coordinates that fall within OpenGL's visible region.
// Extra: In graphics programming we use the mathematical concept of vector quite often, since it neatly represents positions/directions in any space and has useful mathematical properties. A vector in GLSL has a maximum size 4 and each of its values can be retrieved via 'vec.x', 'vec.y', 'vec.z' and 'vec.w' respectively where each of them represents a coordinate in space. Note that the vec.w component is not used as a position in space (we're dealing with 3D not 4D) but is used for something called "perspective division".


// The fragment shader is the second and final shader we're going to create for rendering a triangle. The fragment shader is all about calculating the color output of your pixels. To keep things simple the fragment shader will always output and orange-ish color.
const char* fragmentShaderSource = "#version 330 core\n"
"out vec4 FragColor;\n"
"void main()\n"
"{\n"
"   FragColor = vec4(1.0f, 0.5f, 0.2f, 1.0f);\n"
"}\n\0";
// The fragment shader only required one output variable and that is a vector of size 4 that defines the final color output that we should calculate ourselves. We can declare output values with the out keyword, that we here promptly names 'FragColor'. Next we simply assign a 'vec4' to the color output as an orange color with an alpha value of 1.0 (1.0 being completely opaque).
// Extra: Colors in computer graphics are represented as an array of 4 values: the red, green, blue and alpha (opacity) component, commonly abbreviated to RGBA. When defining a color in OpenGL or GLSL we set the strength of each component to a value between 0.0 and 1.0. If, for example, we would set red to 1.0 and green to 1.0 we would get a mixture of both colors and get the color yellow. Given those 3 color components we can generate over 16 million different colors!

int main()
{
    // GLFW: Initialize and configure
    // ------------------------------
    // Read about this from "01_Hello_Window"
    glfwInit();
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

#ifdef __APPLE__
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
#endif

    // GLFW window creation
    // --------------------
    // Read about this from "01_Hello_Window"
    GLFWwindow* window = glfwCreateWindow(SCR_WIDTH, SCR_HEIGHT, "LearnOpenGL", NULL, NULL);
    if (window == NULL)
    {
        std::cout << "Failed to create GLFW window" << std::endl;
        glfwTerminate();
        return -1;
    }
    glfwMakeContextCurrent(window);
    glfwSetFramebufferSizeCallback(window, framebuffer_size_callback);

    // GLAD: load all OpenGL function pointers
    // ---------------------------------------
    // Read about this from "01_Hello_Window"
    if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
    {
        std::cout << "Failed to initialize GLAD" << std::endl;
        return -1;
    }


    // Build and compile our shader program
    // ------------------------------------
    // VERTEX SHADER
    // In order for OpenGL to use the shader it has to dynamically compile it at run-time from its source code. The first Thing we need to do is create a shader object, again referenced by an ID. So we store the vertex shader as an 'unsigned int' and create the shader with 'glCreateShader':
    unsigned int vertexShader = glCreateShader(GL_VERTEX_SHADER);
    // We provide the type of shader we want to create as an argument to 'glCreateShader'. Since we're creating a vertex shader we pass in 'GL_VERTEX_SHADER'.

    // Next we attach the shader source code to the shader object and compile the shader:
    glShaderSource(vertexShader, 1, &vertexShaderSource, NULL);
    glCompileShader(vertexShader);
    // The 'glShaderSource' function takes the shader object to compile as its first argument. The second argument specifies how many strings we're passing as source code, which is only one. The third parameter is the actual source code of the vertex shader and we can leave the 4th parameter to NULL.

    // CHECK FOR SHADER COMPILE ERRORS
    // We probably want to check if compilation was successful after the call to 'glCompileShader' and if not, what errors were found so you can fix those. Checking for compile-time errors is accomplished as follows:
    int success;
    char infoLog[512];
    glGetShaderiv(vertexShader, GL_COMPILE_STATUS, &success);
    // First we define an integer to indicate success and a storage container for the error messages (if any). Then we check if compilation was successful with 'glGetShaderiv'. If compilation failed, we should retrieve the error message with 'glGetShaderInfoLog' and print the error message.
    if (!success)
    {
        glGetShaderInfoLog(vertexShader, 512, NULL, infoLog);
        std::cout << "ERROR::SHADER::VERTEX::COMPILATION_FAILED\n" << infoLog << std::endl;
    }
    // If no errors were detected while compiling the vertex shader it is now compiled.

    // FRAGMENT SHADER
    // The process for compiling a fragment shader is similar to the vertex shader, although this time we use the 'GL_FRAGMENT_SHADER' constant as the shader type:
    unsigned int fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(fragmentShader, 1, &fragmentShaderSource, NULL);
    glCompileShader(fragmentShader);
    // CHECK FOR SHADER COMPILE ERRORS
    // Make sure to check for compile errors here as well!
    glGetShaderiv(fragmentShader, GL_COMPILE_STATUS, &success);
    if (!success)
    {
        glGetShaderInfoLog(fragmentShader, 512, NULL, infoLog);
        std::cout << "ERROR::SHADER::FRAGMENT::COMPILATION_FAILED\n" << infoLog << std::endl;
    }

    // Both the shaders are now compiled and the only thing left to do is link both shader objects into a "shader program" that we can use for rendering.

    // A shader program object is the final linked version of multiple shaders combined. To use the recently compiled shaders we use to link them to a shader program object and then activate this shader program when rendering objects. The activated shader program's shaders will be used when we issue render calls.
    // When linking the shaders into a program it links the output of each shader to the inputs of the next shader. This is also where you'll get linking errors if your outputs and inputs do not match.

    // LINK SHADERS
    // Creating a program object is easy:
    unsigned int shaderProgram = glCreateProgram();
    // The 'glCreateProgram' function creates a program and returns the ID reference to the newly created program object. Now we need to attach the previously compiled shaders to the program object and then link them with 'glLinkProgram':

    glAttachShader(shaderProgram, vertexShader);
    glAttachShader(shaderProgram, fragmentShader);
    glLinkProgram(shaderProgram);
    // This code should be pretty self-explanatory, we attach the shaders to the program and link them via 'glLinkProgram'.

    // CHECK FOR LINKING ERRORS
    // Just like shader compilation we can also check if linking a shader program failed and retrieve the corresponding log. However, instead of using 'glGetShaderiv' and 'glGetShaderInfoLog' we now use:
    glGetProgramiv(shaderProgram, GL_LINK_STATUS, &success);
    if (!success) {
        glGetProgramInfoLog(shaderProgram, 512, NULL, infoLog);
        std::cout << "ERROR::SHADER::PROGRAM::LINKING_FAILED\n" << infoLog << std::endl;
    }
    // The result is a program object we can activate by calling 'glUseProgram' with the newly created program object as its argument:
    // glUseProgram(shaderProgram);
    // Every shader and rendering call after 'glUseProgram' will now use this program object (and thus the shaders).

    // Oh yeah, and don't forget to delete the shader objects once we've linked them into the program object; we no longer need them anymore:
    glDeleteShader(vertexShader);
    glDeleteShader(fragmentShader);

    // Right now we sent the input vertex data to the GPU and instructed the GPU how it should process the vertex data within a vertex and fragment shader. We're almost there, but not quite yet. OpenGL does not yet know how it should interpret the vertex data in memory and how it should connect the vertex data to the vertex shader's attributes. We'll be nice and tell OpenGL how to do that.


    // Set up vertex data (and buffer(s)) and configure vertex attributes
	// ------------------------------------------------------------------

    // To start drawing something we will have to first give OpenGL some input vertex data. OpenGL is a 3D graphics library so all coordinates that we specify in OpenGL are in 3D (x, y and z coordinate). OpenGL doesn't simply transform all your 3D coordinates to 2D pixels on your screen; OpenGL only processes 3D coordinates when they're in a specific range between -1.0 and 1.0 on all 3 axes (x, y and z). All coordinates within this so called normalized device coordinates range will end up visible on your screen (and all coordinates outside this region won't)

    // Because we want to render a single triangle we want to specify a total of three vertices with each vertex having a 3D position. We define them in normalized device coordinates (the visible region of OpenGL) in a float array:

    float vertices[] = {
        -0.5f, -0.5f, 0.0f,
         0.5f, -0.5f, 0.0f,
         0.0f,  0.5f, 0.0f
    };
    // Extra 1: Because OpenGL works in a 3D space we render a 2D triangle with each vertex having a z coordinate of 0.0. This way the depth of the triangle remains the same making it look like it's 2D.
    // Extra 2: Unlike usual screen coordinates the positive y-axis points in the up-direction and the (0,0) coordinates are at the center of the window/viewport, instead of top-left.

    // With the vertex data defined we'd like to send it as input to the first process of the graphics pipeline: the vertex shader. This is done by creating memory on the GPU where we store the vertex data, configure how OpenGL should interpret the memory and specify how to send the data to the graphics card. The vertex shader then processes as much vertices as we tell it to from its memory.
    // We manage this memory via so called "vertex object buffer objects" (VBO) that can store a large number of vertices in the GPU's memory. The advantage og using those buffer objects is that we can send large batches of data all at once to the graphics card, and keep it there if there's enough memory left, without having to send data one vertex at a time. Sending data to the graphics card from the CPU is relatively slow, so whenever we can we try to send as much data as possible at once. Once the data is in the graphics card's memory the vertex shader has almost instant access to the vertices making it extremely fast.




    // A "vertex array object" (also known as VAO) can be bound using 'glBindVertexArray' and any subsequent vertex attribute calls from that point on will be stored inside the VAO. This has the advantage that when configuring vertex attribute pointers you only have to make those calls once and whenever we want to draw the object, we can just bind the corresponding VAO. This makes switching between different vertex data and attribute configurations easy as binding a different VAO. All the state we just set is stored inside the VAO.

    // A vertex array object stores the following:
    // * Calls to 'glEnableVertexAttribArray' or 'glDisableVertexAttribArray'.
    // * Vertex attribute configurations via 'glVertexAttribPointer'
    // * Vertex buffer objects associated with vertex attributes by calls to 'glVertexAttribPointer'

    // A vertex array object is an OpenGL object. Just like any object in OpenGL, this vertex array has a unique ID corresponding to that array, so we can generate one with a vertex array ID using the 'glGenVertexArrays' function:
    unsigned int VAO;
    glGenVertexArrays(1, &VAO);

    // To use a VAO all you have to do is bind the VAO using 'glBindVertexArray'. From that point on we should bind/configure the corresponding VBO(s) and attribute pointer(s) and then unbind the VAO for later use. As soon as we want to draw an object, we simply bind the VAO with the preferred settings before drawing the object and that is it. 
    // Bind the Vertex Array Object first, then bind and set vertex buffer(s), and then configure vertex attributes(s).
    glBindVertexArray(VAO);


    // A vertex buffer object is an OpenGL object. Just like any object in OpenGL, this buffer has a unique ID corresponding to that buffer, so we can generate one with a buffer ID using the 'glGenBuffers' function:
    // The process to generate a VBO looks similar to that of a VAO:
    unsigned int VBO;
    glGenBuffers(1, &VBO);


    // OpenGL has many types of buffer objects and the buffer type of a vertex buffer object is 'GL_ARRAY_BUFFER'. OpenGL allows us to bind to several buffers at once as long as they have a different buffer type. We can bind the newly created buffer to the 'GL_ARRAY_BUFFER' target with the 'glBindBuffer' function:
    glBindBuffer(GL_ARRAY_BUFFER, VBO);

    // From that point on any buffer calls we make (on the 'GL_ARRAY_BUFFER' target) will be used to configure he currently bound buffer, which is 'VBO'. Then we can make a call to the 'glBufferData' function that copies the previously defined vertex data into the buffer's memory:
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);
    // 'glBufferData' is a function specifically targeted to copy user-defined data into the currently bound buffer. Its first argument is the type of buffer we want to copy data into: the vertex buffer object currently bound to the GL_ARRAY_BUFFER target. The second argument specifies the size of the data (in bytes) we want to pass to the buffer; a simple 'sizeof' of the vertex data suffices. The third parameter is the actual data we want to send.
    // The fourth parameter specifies how we want the graphics card to manage the given data. This can take 3 forms:
    //		'GL_STREAM_DRAW': the data is set only once and used by the GPU at most a few times
    //		'GL_STATIC_DRAW': the data is set only once and used many times.
    //		'GL_DYNAMIC_DRAW': The data is changed a lot and used many times.
    // The position data of the triangle does not change, is used a lot, and stays the same for every render call so its usage type should best be 'GL_STATIC_DRAW'. if, for instance, one would have a buffer with data that is likely to change frequently, a usage type of 'GL_DYNAMIC_DRAW' ensures the graphics card will place the data in memory that allows for faster writes.



    
    


    // LINKING VERTEX ATTRIBUTES
    // The vertex shader allows us to specify any input we want in the form of vertex attributes and while this allows for great flexibility, it does mean we have to manually specify what part of our input data goes to which vertex attribute in the vertex shader. This means we have to specify how OpenGL should interpret the vertex data before rendering.
    // Our vertex buffer data is formatted as follows:
    // 
    //           -------------------------------------------------------
    //           |    VERTEX  1    |    VERTEX  2    |    VERTEX  3    |
    //           |-----------------|-----------------|-----------------|
    //           |  X  |  Y  |  Z  |  X  |  Y  |  Z  |  X  |  Y  |  Z  |
    //           -------------------------------------------------------
    //           |     |     |     |     |     |     |     |     |     |
    //     Byte: 0     4     8    12    16    20    24    28    32    36
    // Position: ----STRIDE:12---->
    //           -OFFSET:0
    // 
    // * The position data is stored as 32-bit (4 byte) floating point values.
    // * Each position is composed of 3 of those values.
    // * There is no space (or other values) between each of the 3 values. The values are tightly packed in the array.
    // * The first value in the data is at the beginning of the buffer.


    // With this knowledge we can tell OpenGL how it should interpret the vertex data (per vertex attribute) using 'glVertexAttribPointer':
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void*)0);
    glEnableVertexAttribArray(0);
    // The function 'glVertexAttribPointer' has quite a few parameters so let's carefully walk through them:
    // * The first parameter specifies which vertex attribute we want to configure. Remember that we specified the location of the 'position' vertex attribute in the shader with 'layout' (location = 0). This sets the location of the vertex attribute to 0 and since we want to pass data to this vertex attribute, we pass in 0.
    // * The next argument specifies the size of the vertex attribute. The vertex attribute is a 'vec3' so it is composed of 3 values.
    // * The third argument specifies the type of data which is 'GL_FLOAT' (a vec* in GLSL consists of floating point values).
    // * The next argument specifies if we want the data to be normalized. If we're inputting integer data types (int, byte) and we've set this to 'GL_TRUE', the integer data is normalized to 0 (or -1 for signed data) and 1 when converted to float. This is not relevant for us so we'll leave this at 'GL_FALSE'.
    // * The fifth argument is known as the "stride" and tells us the space between consecutive vertex attributes. Since the next set og position data is located exactly 3 times the size of a float away we specify that value as the stride. Note that since we know that the array is tightly packed (there is no space between the next vertex attribute value) we could've also specified the stride as 0 to let OpenGL determine the stride (this only works when values are tightly packed). Whenever we have more vertex attributes we have to carefully define the spacing between each vertex attribute.
    // * The last parameter is of type void* and thus requires that weird cast. This is the "offset" of where the position data begins in the buffer. Since the position data is at the start of the data array this value is just 0.


    // note that this is allowed, the call to 'glVertexAttribPointer' registered VBO as the vertex attribute's bound vertex buffer object so afterwards we can safely unbind
    glBindBuffer(GL_ARRAY_BUFFER, 0);


    // You can unbind the VAO afterwards so other VAO calls won't accidentally modify this VAO, but this rarely happens. Modifying other
    // VAOs requires a call to glBindVertexArray anyways so we generally don't unbind VAOs (nor VBOs) when it's not directly necessary.
    glBindVertexArray(0);




    // In code this would look a bit like this:
    /*
        // ..:: Initialization code (done once (unless your object frequently changes)) :: ..
        // 1. bind Vertex Array Object
        glBindVertexArray(VAO);
        // 2. copy our vertices array in a buffer for OpenGL to use
        glBindBuffer(GL_ARRAY_BUFFER, VBO);
        glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);
        // 3. then set our vertex attributes pointers
        glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void*)0);
        glEnableVertexAttribArray(0);


        [...]

        // ..:: Drawing code (in render loop) :: ..
        // 4. draw the object
        glUseProgram(shaderProgram);
        glBindVertexArray(VAO);
        someOpenGLFunctionThatDrawsOurTriangle();
    */

    // And that is it! Everything we did the last few million pages led up to this moment, a VAO that stores our vertex attribute configuration and which VBO to use. Usually when you have multiple objects you want to draw, you first generate/configure all the VAO's (and thus the required VBO and attribute pointers) and store those for later use. The moment we want to draw one of our objects, we take the corresponding VAO, bind it, then draw the object and unbind the VAO again.



    // Render loop
    // -----------
    // Read about this from "01_Hello_Window"
    while (!glfwWindowShouldClose(window))
    {
        // Input
        // -----
        processInput(window);

        // Render
        // ------
        // Read about this from "01_Hello_Window"
        glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT);

        // Draw our first triangle
        glUseProgram(shaderProgram);
        glBindVertexArray(VAO); // Seeing as we only have a single VAO there's no need to bind it every time, but we'll do so to keep things a bit more organized

        // To draw our objects of choice, OpenGL provides us with the 'glDrawArrays' function that draws primitives using the currently active shader, the previously defined vertex attribute configuration and with the VBO's vertex data (indirectly bound via the VAO).
        glDrawArrays(GL_TRIANGLES, 0, 3);
        // The 'glDrawArrays' function takes as its first argument the OpenGL primitive type we would like to draw. Since i said at the start we wanted to draw a triangle, and i don't like lying to you, we pass in 'GL_TRIANGLES'. The second argument specifies the starting index of the array we'd like to draw; we just leave this at 0. This last argument specifies how many vertices we want to draw, which is 3 (we only render 1 triangle from out data, which is exactly 3 vertices long).

        // glBindVertexArray(0); // no need to unbind it every time 

        // GLFW: Swap buffers and poll IO events (key pressed/released, mouse moved etc.)
        // ------------------------------------------------------------------------------
        // Read about this from "01_Hello_Window"
        glfwSwapBuffers(window);
        glfwPollEvents();
    }

    // Optional: de-allocate all resources once they've outlived their purpose:
    // ------------------------------------------------------------------------
    glDeleteVertexArrays(1, &VAO);
    glDeleteBuffers(1, &VBO);
    glDeleteProgram(shaderProgram);

    // GLFW: Terminate, clearing all previously allocated GLFW resources.
    // ------------------------------------------------------------------
    // Read about this from "01_Hello_Window"
    glfwTerminate();
    return 0;
}

// GLFW: whenever the window size changed (by OS or user resize) this callback function executes
// ---------------------------------------------------------------------------------------------
void framebuffer_size_callback(GLFWwindow* window, int width, int height)
{
    // make sure the viewport matches the new window dimensions; note that width and 
    // height will be significantly larger than specified on retina displays.
    glViewport(0, 0, width, height);
}

// Process all input: query GLFW whether relevant keys are pressed/released this frame and react accordingly
// ---------------------------------------------------------------------------------------------------------
void processInput(GLFWwindow* window)
{
    if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
        glfwSetWindowShouldClose(window, true);
}