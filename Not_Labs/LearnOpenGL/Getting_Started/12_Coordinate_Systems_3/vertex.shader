#version 330 core
layout (location = 0) in vec3 aPos;       // The position variable has attribute position 0
layout (location = 1) in vec2 aTexCoord;  // The coordinate variable has attribute position 1

out vec2 TexCoord;  // Input a coordinate to the fragment shader


uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;

void main()
{
    // Now that we created the transformation matrices we should pass them to our shaders. First let's declare the transformation matrices as uniform in the vertex shader and multiply them with the vertex coordinates:
    gl_Position = projection * view * model * vec4(aPos, 1.0f);
    // Note that we read the multiplication from right to left

    TexCoord = aTexCoord;
}