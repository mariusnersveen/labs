// https://learnopengl.com/Getting-started/Hello-Window

// GLAD manages function pointers for OpenGL
#include <glad/glad.h>

// GLFW is a library targeted at OpenGL
// GLFW gives us the bare necessities required for rendering goodies to the screen. It allows us to create and OpenGL context, define window parameters, and handle user input, which is plenty enough for our purpose.
#include <glfw/glfw3.h>

// For printing to console (cout)
#include <iostream>

// The moment a user resizes the window the viewport should be adjusted as well. We can register a callback function on the window that gets called each time the window is resized.
// The framebuffer size function takes a 'GLFWwindow' as its first argument and two integers indication the new window dimensions. Whenever the window changes in size, GLFW calls this function and fills in the proper arguments for you to process.
void framebuffer_size_callback(GLFWwindow* window, int width, int height);

// We want to have some form of input control in GLFW and we can achieve this with several of GLFW's input functions. We'll be using GLFW's 'glfwGetKey' function that takes the window as input together with a key. This function returns whether this key is currently being pressed. We're creating 'processInput' function to keep all input code organized:
void processInput(GLFWwindow* window);

// Settings
const unsigned int SCR_WIDTH = 800;
const unsigned int SCR_HEIGHT = 600;

int main()
{

	// GLFW: Initialize and configure
	// ------------------------------
	// Initialize GLFW
	glfwInit();

	// After initializing we can configure GLFW
	// The first argument tells us what option we want to configure, the second argument is an intiger that sets the value of our option.
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

	// Checking if we're on a apple device
#ifdef __APPLE__
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE); // Need to add this for Mac OS X to work
#endif


	// GLFW window creation
	// --------------------
	// Create a window object to hold all the windowing data
	// the first three arguments are (width, height, window name,... )
	GLFWwindow* window = glfwCreateWindow(SCR_WIDTH, SCR_HEIGHT, "LearnOpenGL", NULL, NULL);
	// Making sure the window object was created successfully
	if (window == NULL)
	{
		std::cout << "Failed to create GLFW window" << std::endl;
		glfwTerminate();
		return -1;
	}
	// Tell GLFW to make the context of our window the main context on the current thread
	glfwMakeContextCurrent(window);

	// Before we can start rendering we have to do one last thing. We have to tell OpenGL the size of the rendering window so OpenGL knows how we want to display the data and coordinates with respect to the window. We can set those dimensions via the glViewport function:
	// The first two parameters set the location of the lower left corner of the window. The third and fourth parameter set the width and height of the rendering window in pixels, which is equal to GLFW's window size.
	// ---------------------------------------------------------------------------------------------
	// glViewport(0, 0, 800, 600); // THIS IS NOT NEEDED BECAUSE THE FOLLOWING FUNCTION WILL ALSO SET THE CORRECT VIEWPORT SIZE WHEN THE WINDOW IS FIRST DISPLAYED
	// ---------------------------------------------------------------------------------------------
	// Extra 1: We could actually set the viewport dimensions at values smaller than GLFW's dimensions; then all the OpenGL rendering would be displayed in a smaller window and we could for example display other elements outside the OpenGL viewport. 
	// Extra 2: Behind the scenes OpenGL uses the data specified via glViewport to transform the 2D coordinates it processed to coordinates on your screen. For example, a processed point of location (-0.5,0.5) would (as its final transformation) be mapped to (200,450) in screen coordinates. Note that processed coordinates in OpenGL are between -1 and 1 so we effectively map from the range (-1 to 1) to (0, 800) and (0, 600). 

	// Tell GLFW to call the function 'framebuffer_size_callback' on every window resize
	glfwSetFramebufferSizeCallback(window, framebuffer_size_callback);


	// GLAD: load all OpenGL function pointers
	// ---------------------------------------
	// We pass GLAD the function to load the address of the OpenGL function pointers which is OS-specific. GLFW gives us glfwGetProcAddress that defines the correct function based on which OS we're compiling for.
	if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
	{
		std::cout << "Failed to initialize GLAD" << std::endl;
		return -1;
	}

	// Render loop
	// -----------
	// GLFW render loop
	// This will keep on running until we tell GLFW to stop.
	// The 'glfwWindowShouldClose' function checks at the start of each loop iteration if GLFW has been instructed to close. If so, the function returns 'true' and the render loop stops running.
	while (!glfwWindowShouldClose(window))
	{
		// Input
		// -----
		// Checking and handling key press
		processInput(window);


		// Render
		// ------
		// Just to test if things actually work we want to clear the screen with a color of our choice. At the start of frame we want to clear the screen. Otherwise we would still see the results from the previous frame (this could be the effect you're looking for, but usually you don't). We can clear the screen's color buffer using 'glClear' where we pass in buffer bits to specify which buffer we would like to clear. The possible bits we can set are 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' and 'GL_STENCIL_BUFFER_BIT'. Right now we only care about the color values so we only clear the color buffer.
		// Note that we also specify the color to clear the screen with using 'glClearColor'. Whenever we call 'glClear' and clear the color buffer, the entire color will be filled with the color as configured by 'glClearColor'. This will result in a dark green-blueish color.
		glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT);
		// Extra: The 'glClearColor function is a state-setting function and 'glClear' is a state-using function in that it uses the current state to retrieve the clearing color from.



		
		// GLFW: Swap buffers and poll IO events (key pressed/released, mouse moved etc.)
		// ------------------------------------------------------------------------------
		// The 'glfwSwapBuffers' will swap the color buffer (a large 2D buffer that contains color values for each pixel in GLFW's window) that is used to render to during this render iterations and show it as output to the screen.
		glfwSwapBuffers(window);
		// Extra: Double buffer
		// When an application draws in a single buffer the resulting image may display flickering issues.This is because the resulting output image is not drawn in an instant, but drawn pixel by pixeland usually from left to rightand top to bottom.Because this image is not displayed at an instant to the user while still being rendered to, the result may contain artifacts.To circumvent these issues, windowing applications apply a double buffer for rendering.The front buffer contains the final output image that is shown at the screen, while all the rendering commands draw to the back buffer.As soon as all the rendering commands are finished we swap the back buffer to the front buffer so the image can be displayed without still being rendered to, removing all the aforementioned artifacts.

		// The 'glfwPollEvents' checks if any events are triggered (like keyboard input or mouse movement events), updates the window state, and calls the corresponding functions (which we can register via callback methods).
		glfwPollEvents();
	}

	// GLFW: Terminate, clearing all previously allocated GLFW resources.
	// ------------------------------------------------------------------
	// As soon as we exit the render loop we would like to properly clean/delete all of GLFW's resources that were allocated. We can do this via the 'glfwTerminate' function that we call at the end of the 'main' function.
	// This will clean up all the resources and properly exit the application.
	glfwTerminate();
	return 0;
}



// GLFW: whenever the window size changed (by OS or user resize) this callback function executes
// ---------------------------------------------------------------------------------------------
void framebuffer_size_callback(GLFWwindow* window, int width, int height)
{
	// make sure the viewport matches the new window dimensions; note that width and 
	// height will be significantly larger than specified on retina displays.
	glViewport(0, 0, width, height);
}

// Process all input: query GLFW whether relevant keys are pressed/released this frame and react accordingly
// ---------------------------------------------------------------------------------------------------------
void processInput(GLFWwindow* window)
{
	// Here we check whether the user has pressed the scape key (if it's not pressed, 'glfwGetKey' returns 'GLFW_RELEASE'). If the user did press the escape key, we close GLFW by setting its WindowShouldCClose property to 'true' using 'glfwSetWindowShouldClose'. The next condition check of the main while loop will fail and the application closes.
	if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
		glfwSetWindowShouldClose(window, true);
}