// Read about ''sampler(2D)'' and the 'texture' function from "07_Textures_1"
// Read about the 'mix' function from "08_Textures_2"
#version 330 core
out vec4 FragColor;

in vec3 ourColor;
in vec2 TexCoord;

uniform sampler2D texture1;
uniform sampler2D texture2;

void main()
{
    // Linearly interpolates between both textures (80% container, 20% awesomeface)
    FragColor = mix(texture(texture1, TexCoord), texture(texture2, TexCoord), 0.2);
}