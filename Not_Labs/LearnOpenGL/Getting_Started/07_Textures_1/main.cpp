// https://learnopengl.com/Getting-started/Textures

// SHADERS
// -------
// Read about shaders from "04_Shaders_1"


// Loading and creating textures
// The first thing we need to do to actually use textures is to load them into our application. Texture images can be stored in dozens of file formats, each with their own structure and ordering of data, so how do we get those images in our application? One solution would be to choose a file format we'd like to use, say .PNG and write out own image loader to convert the image format into a large array of bytes. While it's not very hard to write your own image loader, it's still cumbersome and what if you want to support more file formats? You'd then have to write and image loader for each format you want to support.
// Another solution, and probably a good one, is to use and image-loading library that supports several popular formats and does all the hard work for us. A library like 'stb_image.h'
// stb_image.h is a very popular single header image loading library by Sean Barrett that is able to load most popular file formats and is easy to integrate in your project(s).
// The stb_image project is added to the "external/stb_image.h" folder
#include <stb_image.h>

// Handle shaders
#include "shader_s.h"

#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include <iostream>
#include <cmath>

// Read about this from "01_Hello_Window"
void framebuffer_size_callback(GLFWwindow* window, int width, int height);

// Read about this from "01_Hello_Window"
void processInput(GLFWwindow* window);

// Settings
const unsigned int SCR_WIDTH = 800;
const unsigned int SCR_HEIGHT = 600;

// VERTEX SHADER
// Read about this from "02_Hello_Triangle"
// The vertex shader is stored in "shader.vs"

// FRAGMENT SHADER
// Read about this from "02_Hello_Triangle"
// Read about "uniforms" from "04_Shaders_1"
// The fragment shader is stored in "shader.fs"

int main()
{
    // GLFW: Initialize and configure
    // ------------------------------
    // Read about this from "01_Hello_Window"
    glfwInit();
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

#ifdef __APPLE__
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
#endif

    // GLFW window creation
    // --------------------
    // Read about this from "01_Hello_Window"
    GLFWwindow* window = glfwCreateWindow(SCR_WIDTH, SCR_HEIGHT, "LearnOpenGL", NULL, NULL);
    if (window == NULL)
    {
        std::cout << "Failed to create GLFW window" << std::endl;
        glfwTerminate();
        return -1;
    }
    glfwMakeContextCurrent(window);
    glfwSetFramebufferSizeCallback(window, framebuffer_size_callback);

    // GLAD: load all OpenGL function pointers
    // ---------------------------------------
    // Read about this from "01_Hello_Window"
    if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
    {
        std::cout << "Failed to initialize GLAD" << std::endl;
        return -1;
    }

    // Prints the max number of vertex attributes supported by the hardware
    // OpenGL guarantees there are always at least 16 4-component vertex attributes available
    int nrAttributes;
    glGetIntegerv(GL_MAX_VERTEX_ATTRIBS, &nrAttributes);
    std::cout << "Maximum nr of vertex attributes supported: " << nrAttributes << std::endl;


    // Build and compile our shader program
    // ------------------------------------
    // The path is the path from the .exe file inside out/build/x64-Debug/bin/
    Shader ourShader("../../../../Not_Labs/LearnOpenGL/Getting_Started/07_Textures_1/shader.vs", "../../../../Not_Labs/LearnOpenGL/Getting_Started/07_Textures_1/shader.fs");

    // Set up vertex data (and buffer(s)) and configure vertex attributes
    // ------------------------------------------------------------------
    // Read about this from "02_Hello_Triangle"
    // Read about how one vertex can contain position and color data in "05_Shaders_2"



    // Texture coords
    // For the upcoming sections we will use the rectangle shape drawn with 'glDrawElements' from the final part of the "Hello_Triangle" chapter. 
    // In order to map a texture to the square we drew in the earlier projects we need to tell each vertx of the square which part of the texture it corresponds to. Each vertex should thus have a texture coordinate associated with them that specifies what part of the texture image to sample from. Fragment interpolation then does the rest for the other fragments.
    // Texture coordinates range from 0 to 1 in the x and y axis (remember that we use 2D texture images). Retrieving the texture color using texture coordinates is called "sampling". Texture coordinates start at (0,0) for the lower left corner of a texture image to (1,1) for the upper right corner of a texture image.
    // We specify 4 texture coordinate points for the square. We want the bottom-left side of the square to correspond with the bottom-left side of the texture so we use the (0,0) texture coordinate for the square's bottom-left vertex. The same applies to the bottom-right side with a (1,0) texture coordinate. The top-right of the square should correspond with the top-right of the texture image so we take (1,1) as its texture coordinate. The top-left of the square should correspond with the top-left of the texture image so we take (0,1). We only have to pass 4 texture coordinates to the vertex shader, which then passes those to the fragment shader that neatly interpolates all the texture coordinates for each fragment.
    // The resulting texture coordinates would then look like this:
    /*
        float texCoords[] = {
            1.0f, 1.0f   // top-right corner
            1.0f, 0.0f,  // lower-right corner
            0.0f, 0.0f,  // lower-left corner
            0.0f, 1.0f,  // top-left corner
        };
    */

    // We need to inform OpenGL how to sample the texture so we'll have to update vertex data with the texture coordinates:
    float vertices[] = {
        // positions          // colors           // texture coords
         0.5f,  0.5f, 0.0f,   1.0f, 0.0f, 0.0f,   1.0f, 1.0f,   // top right
         0.5f, -0.5f, 0.0f,   0.0f, 1.0f, 0.0f,   1.0f, 0.0f,   // bottom right
        -0.5f, -0.5f, 0.0f,   0.0f, 0.0f, 1.0f,   0.0f, 0.0f,   // bottom left
        -0.5f,  0.5f, 0.0f,   1.0f, 1.0f, 0.0f,   0.0f, 1.0f    // top left
    };

    unsigned int indices[] = {
        0, 1, 3, // first triangle
        1, 2, 3  // second triangle
    };

    // Generating array and buffers
    unsigned int VAO, VBO, EBO;
    glGenVertexArrays(1, &VAO);
    glGenBuffers(1, &VBO);
    glGenBuffers(1, &EBO);

    // Bind the Vertex Array Object first, then bind and set vertex buffer(s), and then configure vertex attributes(s).
    glBindVertexArray(VAO);

    glBindBuffer(GL_ARRAY_BUFFER, VBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);


    // LINKING VERTEX ATTRIBUTES
    // Read about this from "02_Hello_Triangle"
    // Read about how one vertex can contain position and color data in "05_Shaders_2"

    // Since we've added an extra vertex attribute we again have to notify OpenGL of  the new vertex format:
    // 
    //           -------------------------------------------------------------------------------------------------------------------------------------------------
    //           |                   VERTEX  1                   |                   VERTEX  2                   |                   VERTEX  3                   |
    //           |-----------------------------------------------|-----------------------------------------------|-----------------------------------------------|
    //           |  X  |  Y  |  Z  |  R  |  G  |  B  |  S  |  T  |  X  |  Y  |  Z  |  R  |  G  |  B  |  S  |  T  |  X  |  Y  |  Z  |  R  |  G  |  B  |  S  |  T  |
    //           -------------------------------------------------------------------------------------------------------------------------------------------------
    //           |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |
    //     Byte: 0     4     8    12    16    20    24    28    32    36    40    44    48    52    56    60    64    68    72    76    80    84    88    92    96
    // Position: .-----------------.STRIDE: 32 ------.---------->|                 .                 .
    //           .OFFSET: 0        .                 .                             .                 .
    //    Color: .                 .-----------------.STRIDE: 32 ----------------->.                 .
    //           .---OFFSET: 12--->.                 .                                               .
    //           .                                   .----------------- STRIDE: 32 ----------------->.
    //  Texture: .------------OFFSET: 24------------>.
    //

    // Position attribute
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)0);
    glEnableVertexAttribArray(0);
    // Color attribute
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)(3 * sizeof(float)));
    glEnableVertexAttribArray(1);
    // Texture coord attribute
    glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)(6 * sizeof(float)));
    glEnableVertexAttribArray(2);
    // Note that we have to adjust the stride parameter of the previous two vertex attributes to 8 * 'sizeof(float)' as well.


    // You can unbind the VAO afterwards so other VAO calls won't accidentally modify this VAO, but this rarely happens. Modifying other
    // VAOs requires a call to glBindVertexArray anyways so we generally don't unbind VAOs (nor VBOs) when it's not directly necessary.
    //glBindVertexArray(0);

    // Bind the VAO (it was already bound, but just to demonstrate): seeing as we only have a single VAO we can 
    // just bind it beforehand before rendering the respective triangle; this is another approach.
    glBindVertexArray(VAO);


    // Load and create a texture
    // -------------------------

    // Generating a texture
    // Like any of the previous objects in OpenGL, textures are referenced with an ID; let's create one
    unsigned int texture;
    glGenTextures(1, &texture);
    // The 'glGenTexture' function first takes as input how many textures we want to generate and stores them in a 'unsigned int' array given as its second argument (in our case just a signle 'unsigned int'). Just like other objects we need to bind it so any subsequent texture commands will configure the currently bound texture:
    glBindTexture(GL_TEXTURE_2D, texture);  // all upcoming GL_TEXTURE_2D operations now have effect on this texture object

    // Set the texture wrapping
    // 
    // Texture sampling has a loose interpretation and can be done in many different ways. It is thus our job to tell OpenGL how it should sample its textures.

    // Texture coordinates usually range from (0,0) to (1,1) but what happens if we specify coordinates outside this range? The default behaviour of OpenGL is to repeat the texture images (we basically just ignore the integer part of the floating point texture coordinate), but there are more options OpenGL offers:
    // * 'GL_REPEAT': The default behaviour for textures. Repeats the image.
    // * 'GL_MIRRORED_REPEAT': Same as 'GL_REPEAT' but mirrors the image with each repeat.
    // * 'GL_CLAMP_TO_EDGE': Clamps the coordinates between 0 and 1. The result is that higher coordinates become clamped to the edge, resulting in a stretched edge pattern.
    // * 'GL_CLAMP_TO_BORDER': Coordinates outside the range are now given a user-specific border color.
    // Each of the options have a different visual output when using texture coordinates outside the default range.

    // Each of the aforementioned options can be set per coordinate axis (s, t (and r if you're using 3D textures) equivalent to x,y,z) with the 'glTexParameter*' function:
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);  // Set texture wrapping to GL_REPEAT (default wrapping method)
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    // The first argument specifies the texture target; we're working with 2D textures so the texture target is 'GL_TEXTURE_2D'. The second argument requires us to tell what option we want to set and for which texture axis; we want to configure it for both the 'S' and the 'T' axis. The last argument requires us to pass in the texture wrapping mode we'd like and in this case OpenGL will set its texture wrapping option on the currently active texture with 'GL_REPEAT'.

    // If we choose the 'GL_CLAMP_TO_BORDER' option we should also specify a border color. This is done using the fv equivalent of the 'glTexParameter' function with 'GL_TEXTURE_BORDER_COLOR' as its option where we pass in a float array if the border's color value:
    /*
        float borderColor[] = { 1.0f, 1.0f, 0.0f, 1.0f };
        glTexParameterfv(GL_TEXTURE_2D, GL_TEXTURE_BORDER_COLOR, borderColor);
    */

    // Set texture filtering parameters
    // Texture coordinates do not depend on resolution but can be any floating point value, thus OpenGL has to figure out which texture pixel (also known as a "texel") to map the texture coordinates to. This becomes especially important if you have a very large object and a low resolution texture. You probably guessed by now that OpenGL has options for this "texture filtering" as well. There are several options available but for now we'll discuss the most important options: 'GL_NEAREST' and 'GL_LINEAR'.
    // 'GL_NEAREST' (also known as "nearest neightbor" or "point filtering") is the default texture filtering method of OpenGL. When set to 'GL_NEAREST', OpenGL selects the texel that venter is closest to the texture coordinate. Given 4 pixels in a square, if the texture coordinate is closer to the center of the top left pixel, that pixel will be chosen as the sampled color.
    // 'GL_LINEAR' (also known as (bi)linear filtering) takes an interpolated value from the texture coordinate's neighbor texels, approximating a color between the texels. The smaller the distance from the texel's center, the more that texel's color contributes to the sampled color.
    // 'GL_NEAREST' results in blocked patterns where we can clearly see the pixels that form the texture while 'GL_LINEAR' produces a smoother pattern where the individual pixels are less visible. 'GL_LINEAR' produces a more realistic output, but some developers prefer a more 8-bit look and as a result pick the 'GL_NEAREST' option.

    // Texture filtering can be set for magnifying and minifying operations (when scaling up or downwards) so you could for example use nearest neighbor filtering when textures are scaled downwards and linear filtering for upscaled textures. We thus have to specify the filtering method for both options via 'glTexParameter*'. The code should look similar to setting the wrapping method:
    /*
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    */

    // Mipmaps
    // Imagine we had a large room with thousands of objects, each with an attached texture. There will be objects far away that have the same high resolution texture attached as the objects close to the viewer. Since the objects are far away and probably only produce a few fragments, OpenGL has difficulties retrieving the right color value for its fragment from the high resolution texture, since it has to pick a texture color for a fragment that spans a large part of the texture. This will produce visible artifacts on small objects, not to mention the waste of memory bandwidth using high resolution textures on small objects.
    // To solve this issue OpenGL uses a concept called "mipmaps" that is basically a collection of texture images where each subsequent texture is twice as small compared to the previous one. The idea behind mipmaps should be easy to understand: after a certain distance threshold from the viewer, OpenGL will use a different mipmap texture that best suits the distance to the object. Because the object is far away, the smaller resolution will not be noticeable to the user. OpenGL is then able to sample the correct texels, and ther's less cache memory involved when sampling that part of the mipmaps.

    // Creating a collection of mipmapped textures for each texture image is cumbersome to do manually, but luckily OpenGL is able to do all the work for us with a single call to 'glGenerateMipmap' after we've created a texture.
    // When switching between mipmaps levels during rendering OpenGL may show some artifacts like sharp edges visible between the two mipmap layers. Just like normal texture filtering, it is also possible to filter between mipmap levels using 'NEAREST' and 'LINEAR' filtering for switching between mipmap levels. To specify the filtering method between mipmap levels we can replace the original filtering methods with one of the following four options:
    // * 'GL_NEAREST_MIPMAP_NEAREST': Takes the nearest mipmap to match the pixel sixe and uses nearest neighbor interpolation for sampling.
    // * 'GL_LINEAR_MIPMAP_NEAREST': Takes the nearest mipmap level and samples that level using linear interpolation.
    // * 'GL_NEAREST_MIPMAP_LINEAR': Linearly interpolates between the two mipmaps that most closely match the size of a pixel and samples the interpolated levels via nearest neighbor interpolation.
    // * 'GL_LINEAR_MIPMAP_LINEAR': Linearly interpolates between the two closest mipmaps and samples the interpolated level via linear interpolation.

    // Just like texture filtering we can set the filtering method to one of the 4 aformentioned methods using 'glTexParameteri':
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    // A common mistake is to set one of the mipmap filtering options as the magnification filter. This doesn't have any effect since mipmaps are primary used for when textures get downscaled: texture magnification doesn't use mipmaps and giving it a mipmap filtering option will generate an OpenGL 'GL_INVALID_ENUM' error code.
    

    // Load image, create texture and generate mipmaps
    int width, height, nrChannels;

    // For the following texture sections we're going to use an image of a wooden container(https://learnopengl.com/img/textures/container.jpg). To load an image using stb_image.h we use its 'stbi_load' function:
    unsigned char* data = stbi_load("../../../../Not_Labs/LearnOpenGL/Getting_Started/07_Textures_1/container.jpg", &width, &height, &nrChannels, 0);
    // The function first takes as input the location of an image file. It then expects you to give three 'ints' as its second, third and fourth argument that stb_image.h will fill with the resulting image's width, height and number of color channels. We need the image's width and height for generating textures later on.
    // Checking if loading image was successful
    if (data)
    {
        // Now that the texture is bound, we can start generating a texture using the previously loaded image data. Textures are generated with 'glTexImage2D':
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, data);
        glGenerateMipmap(GL_TEXTURE_2D);
        // This is a large function with quite a few parameters so we'll walk through them step-by-step:
        // * The first argument specifies the texture target; setting this to 'GL_TEXTURE_2D' means this operation will generate a texture on the currently bound texture object at the same target (so any textures bound to targets 'GL_TEXTURE_1D' or 'GL_TEXTURE_3D' will not be affected).
        // * The second argument specifies the mipmap level for which we want to create a texture for if you want to set each mipmap level manually, but we'll leave it at the base level which is 0.
        // * The third argument tells OpenGL in what kind of format we want to store the texture. Our image has only RGB values so we'll store the texture with RGB values as well.
        // * The 4th and 5th argument sets the width and height of the resulting texture. We stored those earlier when loading the image so we'll use the corresponding variables.
        // * The next argument should always be 0 (some legacy stuff).
        // * The 7th and 8th argument specify the format and datatype of the source image. We loaded the image with RGB values and stored them as 'chars' (bytes) so we'll pass in the corresponding values.
        // * The last argument is the actual image data.
        // Once 'glTexImage2D' is called, the currently bound texture object now has the texture image attached to it. However, currently it only has the base-level of the texture image loaded and if we want to use mipmaps we have to specify all the different images manually (by continually incrementing the second argument) or, we could call 'glGenerateMipmap' after generating the texture. This will automatically generate all the required mipmaps for the currently bound texture.
    }
    else
    {
        std::cout << "Failed to load texture" << std::endl;
    }
    // After we're done generating the texture and its corresponding mipmaps, it is good practice to free the image memory:
    stbi_image_free(data);


    // The whole process of generating a texture thus looks something like this:
    /*
        unsigned int texture;
        glGenTextures(1, &texture);
        glBindTexture(GL_TEXTURE_2D, texture);
        // set the texture wrapping/filtering options (on the currently bound texture object)
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        // load and generate the texture
        int width, height, nrChannels;
        unsigned char *data = stbi_load("container.jpg", &width, &height, &nrChannels, 0);
        if (data)
        {
            glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, data);
            glGenerateMipmap(GL_TEXTURE_2D);
        }
        else
        {
            std::cout << "Failed to load texture" << std::endl;
        }
        stbi_image_free(data);
    */


    // Render loop
    // -----------
    // Read about this from "01_Hello_Window"
    while (!glfwWindowShouldClose(window))
    {
        // Input
        // -----
        processInput(window);

        // Render
        // ------
        // Read about this from "01_Hello_Window"
        glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT);
        
        // All that's left to do now is to bind the texture before calling 'glDrawElements' and it will then automatically assign the texture to the fragment shader's sampler:
        glBindTexture(GL_TEXTURE_2D, texture);

        // Render container
        // Be sure to activate the shader before any calls to 'glUniform'
        ourShader.use();
        //glBindVertexArray(VAO);  // This is not needed since we only have 1 vertex array and we bounded it before the render loop
        glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);

        // GLFW: Swap buffers and poll IO events (key pressed/released, mouse moved etc.)
        // ------------------------------------------------------------------------------
        // Read about this from "01_Hello_Window"
        glfwSwapBuffers(window);
        glfwPollEvents();
    }
    // Optional: de-allocate all resources once they've outlived their purpose:
    // ------------------------------------------------------------------------
    glDeleteVertexArrays(1, &VAO);
    glDeleteBuffers(1, &VBO);
    glDeleteBuffers(1, &EBO);

    // GLFW: Terminate, clearing all previously allocated GLFW resources.
    // ------------------------------------------------------------------
    // Read about this from "01_Hello_Window"
    glfwTerminate();
    return 0;
}

// GLFW: whenever the window size changed (by OS or user resize) this callback function executes
// ---------------------------------------------------------------------------------------------
void framebuffer_size_callback(GLFWwindow* window, int width, int height)
{
    // make sure the viewport matches the new window dimensions; note that width and 
    // height will be significantly larger than specified on retina displays.
    glViewport(0, 0, width, height);
}

// Process all input: query GLFW whether relevant keys are pressed/released this frame and react accordingly
// ---------------------------------------------------------------------------------------------------------
void processInput(GLFWwindow* window)
{
    if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
        glfwSetWindowShouldClose(window, true);
}