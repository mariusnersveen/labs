// The fragment shader should also have access to the texture object, but how do we pass the texture object to the fragment shader? GLSL has a built-in data-type for texture objects called a "sampler" that takes as a postfix the type we want e.g. 'sampler1D', 'sampler3D' or in our case 'sampler2D', We can then add a texture to the fragment shader by simply declaring a 'uniform sampler2D' that we later assign out texture to.
#version 330 core
out vec4 FragColor;

in vec3 ourColor;
in vec2 TexCoord;

uniform sampler2D ourTexture;

void main()
{
    // FragColor = texture(ourTexture, TexCoord);  // Only using texture colors
    FragColor = texture(ourTexture, TexCoord) * vec4(ourColor, 1.0);  // Mixing the texture color with the vertex colors
}
// To sample the color of a texture we use GLSL's built-in 'texture' function that takes as its first argument a texture sampler and as its second argument the corresponding coordinates. The 'texture' function then samples the corresponding color values using the texture parameters we set earlier. The output of this fragment shader is then the (filtered) color of t he texture at the (interpolated) texture coordinate.