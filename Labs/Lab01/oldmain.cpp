#include <string>
#include <iostream>
#include <algorithm>
#include <tclap/CmdLine.h>

// St�r mer her http://tclap.sourceforge.net/manual.html

int main(int argc, char** argv)
{

	// Wrap everything in a try block.  Do this every time, 
	// because exceptions will be thrown for problems.
	try {

		TCLAP::CmdLine cmd("Command description message", ' ', "0.9");


		TCLAP::SwitchArg consoleSwitch("c", "console", "Opens program in console mode", cmd, false);

		// Parse the argv array.
		cmd.parse(argc, argv);

		bool openInConsole = consoleSwitch.getValue();

		// Do what you intend. 
		if (openInConsole)
		{
			std::cout << "Launched in console mode" << std::endl;
		}
		else
			std::cout << "Not launched in console mode" << std::endl;


	}
	catch (TCLAP::ArgException& e)  // catch exceptions
	{
		std::cerr << "error: " << e.error() << " for arg " << e.argId() << std::endl;
	}
}


/*
#include <iostream>

int main(int argc, char **argv)
{
  std::cout << "Hello OpenGL!" << std::endl;
  std::cin.get();
  return EXIT_SUCCESS;
}
*/