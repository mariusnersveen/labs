// #include area (SECTION 1)
#include <iostream>
#include <string>
#include <tclap/CmdLine.h>
#include <glad/glad.h>
#include <GLFW/glfw3.h>


void processInput(GLFWwindow* window)
{
	if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
		glfwSetWindowShouldClose(window, true);
}



int main(int argc, char** argv)
{
	bool console, ifTriangle, ifSquare;
	int width, height;
	float currentTime, sinTime, cosTime, rsinTime, rcosTime, hsinTime;

	//
	// Command-line parsing code (SECTION 2)
	//
	try 
	{
		// cmd(printed last in the help text, delimiter, version number, disable default arguments)
		TCLAP::CmdLine cmd("Command description message", ' ', "0.9", false);

		// SwitchArg is a boolean. (flag, name, description, add to command line object, default value)
		TCLAP::SwitchArg consoleSwitch("c", "console", "print \": console\"", cmd, false);
		TCLAP::SwitchArg triangleSwitch("t", "triangle", "print triangle", cmd, true);
		TCLAP::SwitchArg squareSwitch("s", "square", "print square", cmd, false);



		TCLAP::ValueArg<int> widthArg("w", "width", "configure the width of the window", false, 700, "int");
		TCLAP::ValueArg<int> heightArg("h", "height", "configure the height of the window", false, 700, "int");

		cmd.add(widthArg);
		cmd.add(heightArg);

		// Parse the argv array.
		cmd.parse(argc, argv);

		console = consoleSwitch.getValue();
		ifTriangle = triangleSwitch.getValue();
		ifSquare = squareSwitch.getValue();
		width = widthArg.getValue();
		height = heightArg.getValue();

	}

	catch (TCLAP::ArgException& e)  // catch exceptions
	{
		std::cerr << "error: " << e.error() << " for arg " << e.argId() << std::endl;
	}


	//
	// GLFW initialization code (SECTION 3)
	//
	if (!glfwInit()) 
	{
		std::cout << "Failed to initialize GLFW" << std::endl;
		glfwTerminate();
		return EXIT_FAILURE;
	}

	// OpenGL initialization code (SECTION 4)
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);

	GLFWwindow* window = glfwCreateWindow(width, height, "HelloOpenGL", NULL, NULL);
	if (window == NULL) 
	{
		std::cout << "Failed to create GLFW window" << std::endl;
		glfwTerminate();
		return EXIT_FAILURE;
	}


	// tell GLFW to make the context of our window the main context on the current thread
	glfwMakeContextCurrent(window);


	// pass glad the function to load the OpenGL function pointers
	gladLoadGLLoader((GLADloadproc)glfwGetProcAddress);

	//
	// OpenGL data transfer code (SECTION 5)
	//
	float triangle[6 * 3] = 
	{
		-0.5f, -0.5f, 0.0f,  0.0f, 0.0f, 0.0f,
		 0.5f, -0.5f, 0.0f,  0.0f, 0.0f, 0.0f,
		 0.0f,  0.5f, 0.0f,  0.0f, 0.0f, 0.0f
	};

	float square[6 * 3 * 2] = 
	{
		-0.5f,  0.5f, 0.0f,  0.0f, 0.0f, 0.0f,
		 0.5f,  0.5f, 0.0f,  0.0f, 0.0f, 0.0f,
		 0.5f, -0.5f, 0.0f,  0.0f, 0.0f, 0.0f,

		-0.5f,  0.5f, 0.0f,  0.0f, 0.0f, 0.0f,
		-0.5f, -0.5f, 0.0f,  0.0f, 0.0f, 0.0f,
		 0.5f, -0.5f, 0.0f,  0.0f, 0.0f, 0.0f
	};



	// Create a vertex array (VAO)
	// VAO stores all of the state needed to supply vertex data
	GLuint vertexArrayId;
	glGenVertexArrays(1, &vertexArrayId);  
	glBindVertexArray(vertexArrayId);   // set active VAO

	// Create a vertex buffer (VBO)
	// VBO is a buffer used to transfer the	data from CPU to GPU
	GLuint vertexBufferId;
	glGenBuffers(1,					// How many buffers should be created?
				 &vertexBufferId);  // Where to place the IDs of the created buffers?

	// Set active VBO
	// Make the vertex buffer active
	glBindBuffer(GL_ARRAY_BUFFER,  // Type of buffer
				 vertexBufferId);  // ID of the target buffer



	// set active VBO
	glBindBuffer(GL_ARRAY_BUFFER,  // Type of buffer
	vertexBufferId);  // ID of the target buffer


	// triangle
	if (ifTriangle)
	{
		// Transfter the vertices data to the GPU
		glBufferData(GL_ARRAY_BUFFER,   // Type of buffer
					 sizeof(triangle),  // Size of buffer (in bytes)
					 triangle,			// Pointer to the buffer
					 GL_DYNAMIC_DRAW);  // Use of the buffer
	}

	// square
	if (ifSquare)
	{
		// Transfter the vertices data to the GPU
		glBufferData(GL_ARRAY_BUFFER,   // Type of buffer
					 sizeof(square),    // Size of buffer (in bytes)
					 square,			// Pointer to the buffer
					 GL_DYNAMIC_DRAW);  // Use of the buffer
	}


	// Set the layout of the bound buffers
	// Create first attribute pointer
	glVertexAttribPointer(0,				  // Attribute Index
						  3,				  // Components
						  GL_FLOAT,			  // Data type
						  GL_FALSE,			  // Normalize data?
						  sizeof(float) * 6,  // Size of one component
						  nullptr);			  // Ptr to the 1st element / ptr to the current element of the component

	// Create second attribute pointer
	glVertexAttribPointer(1,				  // Attribute Index
						  3,				  // Components
						  GL_FLOAT,			  // Data type
						  GL_FALSE,			  // Normalize data?
						  sizeof(float) * 6,  // Size of one component
						  (void*)12);		  // Ptr to the 1st element / ptr to the current element of the component

	glEnableVertexAttribArray(0);  // Enable attribute 0
	glEnableVertexAttribArray(1);  // Enable attribute 1


	// Vertex shader code
	const std::string vertexShaderSrc = R"(
	#version 430 core

	layout(location = 0) in vec2 position;
	layout (location = 1) in vec3 color;
	out vec3 vertexColor;

	void main()
	{
		gl_Position = vec4(position, 0.0, 1.0);
		vertexColor = color;
	}
	)";

	// Fragment shader code
	const std::string fragmentShaderSrc = R"(
	#version 430 core

	out vec4 color;
	in vec3 vertexColor;

	void main()
	{
		color = vec4(vertexColor, 1.0);
		//color = vec4(1.0, 1.0, 1.0, 1.0);
	}
	)";

	// compile the vertex shader
	unsigned int vertexShader;
	vertexShader = glCreateShader(GL_VERTEX_SHADER);
	const GLchar* vss = vertexShaderSrc.c_str();
	glShaderSource(vertexShader, 1, &vss, NULL);
	glCompileShader(vertexShader);
	
	// check for shader compile errors
	int success;
	char infoLog[512];
	glGetShaderiv(vertexShader, GL_COMPILE_STATUS, &success);
	if (!success) {
		glGetShaderInfoLog(vertexShader, 512, NULL, infoLog);
		std::cout << "ERROR::SHADER::VERTEX::COMPILATION_FAILED\n" << infoLog << std::endl;
	}

	// compile the fragment shader
	unsigned int fragmentShader;
	fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
	const GLchar* fss = fragmentShaderSrc.c_str();
	glShaderSource(fragmentShader, 1, &fss, NULL);
	glCompileShader(fragmentShader);

	// check for shader compile errors
	glGetShaderiv(vertexShader, GL_COMPILE_STATUS, &success);
	if (!success) {
		glGetShaderInfoLog(vertexShader, 512, NULL, infoLog);
		std::cout << "ERROR::SHADER::FRAGMENT::COMPILATION_FAILED\n" << infoLog << std::endl;
	}

	// create a shader program
	unsigned int shaderProgram;
	shaderProgram = glCreateProgram();
	glAttachShader(shaderProgram, vertexShader);
	glAttachShader(shaderProgram, fragmentShader);
	glLinkProgram(shaderProgram);

	glDeleteShader(vertexShader);
	glDeleteShader(fragmentShader);

	glUseProgram(shaderProgram);


	glClearColor(0.5f, 0.0f, 0.0f, 1.0f);

	//
	// Application loop code (SECTION 6)
	//
	while (!glfwWindowShouldClose(window))
	{

		currentTime = glfwGetTime();
		sinTime = sin(currentTime) / 2 + 0.5;
		cosTime = cos(currentTime) / 2 + 0.5;
		rsinTime = -sin(currentTime) / 2 + 0.5;
		rcosTime = -cos(currentTime) / 2 + 0.5;
		hsinTime = sin(currentTime * 2) / 2 + 0.5;

		//glClearColor(hsinTime, hsinTime, hsinTime, hsinTime);

		// Clear
		glClear(GL_COLOR_BUFFER_BIT);


		if (ifTriangle)
		{
			float newColor1[3] = { cosTime, sinTime, 0.0 };
			float newColor2[3] = { sinTime, 0.0, cosTime };
			float newColor3[3] = { 0.0, cosTime, sinTime };
			glBindBuffer(GL_ARRAY_BUFFER, vertexBufferId);
			glBufferSubData(GL_ARRAY_BUFFER, 12, 12, newColor1);
			glBufferSubData(GL_ARRAY_BUFFER, 36, 12, newColor2);
			glBufferSubData(GL_ARRAY_BUFFER, 60, 12, newColor3);

			// Set the state of the pipeline
			glBindVertexArray(vertexArrayId);
			glUseProgram(shaderProgram);

			// Issue the draw call
			glDrawArrays(GL_TRIANGLES,  // Primitive type
						 0,				// First element in buffer
						 3);			// Number of elements to render
		}
		if (ifSquare)
		{
			float newColor1[3] = { cosTime, sinTime, rsinTime };
			float newColor2[3] = { sinTime, rsinTime, rcosTime };
			float newColor3[3] = { rsinTime, rcosTime, sinTime };
			float newColor4[3] = { rcosTime, sinTime, rsinTime };
			glBindBuffer(GL_ARRAY_BUFFER, vertexBufferId);
			glBufferSubData(GL_ARRAY_BUFFER, 12, 12, newColor1);
			glBufferSubData(GL_ARRAY_BUFFER, 36, 12, newColor2);
			glBufferSubData(GL_ARRAY_BUFFER, 60, 12, newColor3);
			glBufferSubData(GL_ARRAY_BUFFER, 84, 12, newColor1);
			glBufferSubData(GL_ARRAY_BUFFER, 108, 12, newColor4);
			glBufferSubData(GL_ARRAY_BUFFER, 132, 12, newColor3);

			// Set the state of the pipeline
			glBindVertexArray(vertexArrayId);
			glUseProgram(shaderProgram);

			// Issue the draw call
			glDrawArrays(GL_TRIANGLES,  // Primitive type
						 0,				// First element in buffer
						 6);			// Number of elements to render
		}

		// Flush / Swap
		glfwSwapBuffers(window);  // SwapBuffers for double buffering

		if (console)
		{
			std::cout << "Hello OpenGL: console" << std::endl;
		}


		glfwPollEvents();
		processInput(window);

	}


	//
	// Termination code (SECTION 7)
	//
	glfwDestroyWindow(window);
	glfwTerminate();

	return EXIT_SUCCESS;
}
