#include <iostream>
#include <tclap/CmdLine.h>
#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <fstream>
#include <sstream>
#include <memory>
#include "Lab02Application.h"
#include <GeometricTools.h>
#include <VertexBuffer.h>
#include <IndexBuffer.h>
#include <ShadersDataTypes.h>
#include <BufferLayout.h>
#include <VertexArray.h>
#include <Shader.h>

Lab02Application::Lab02Application(const std::string& name_, const std::string& version_)
	: GLFWApplication(name_, version_), ifTriangle(false), ifSquare(false) {}

Lab02Application::~Lab02Application() {}

unsigned int Lab02Application::ParseArguments(int argc, char** argv)
{

	try {
		// cmd(printed last in the help text, delimiter, version number, disable default arguments)
		TCLAP::CmdLine cmd("Command description message", ' ', "0.9", false);

		// SwitchArg is a boolean. (flag, name, description, add to command line object, default value)
		TCLAP::SwitchArg consoleSwitch("c", "console", "print \": console\"", cmd, false);
		TCLAP::SwitchArg triangleSwitch("t", "triangle", "print triangle", cmd, false);
		TCLAP::SwitchArg squareSwitch("s", "square", "print square", cmd, false);

		TCLAP::ValueArg<int> widthArg("w", "width", "configure the width of the window", false, 600, "int");
		TCLAP::ValueArg<int> heightArg("h", "height", "configure the height of the window", false, 600, "int");

		cmd.add(widthArg);
		cmd.add(heightArg);

		// Parse the argv array.
		cmd.parse(argc, argv);

		ifTriangle = triangleSwitch.getValue();
		ifSquare = squareSwitch.getValue();
		width = widthArg.getValue();
		height = heightArg.getValue();

	}
	catch (TCLAP::ArgException& e) {
		std::cerr << "error: " << e.error() << " for arg " << e.argId() << std::endl;
		return 1;
	}
	return 0;
}

struct ShaderProgramSource
{
	std::string VertexSource;
	std::string FragmentSource;
};

static ShaderProgramSource ParseShader(const std::string& filepath)
{
	std::ifstream stream(filepath);

	enum class ShaderType { NONE = -1, VERTEX = 0, FRAGMENT = 1 };

	std::string line;
	std::stringstream ss[2];
	ShaderType type = ShaderType::NONE;
	while (getline(stream, line)) {
		if (line.find("#shader") != std::string::npos) {
			if (line.find("vertex") != std::string::npos) {
				type = ShaderType::VERTEX;
			}
			else if (line.find("fragment") != std::string::npos) {
				type = ShaderType::FRAGMENT;
			}
		}
		else {
			ss[(int)type] << line << "\n";
		}
	}

	return { ss[0].str(), ss[1].str() };
}

static void processInput(GLFWwindow* window)
{
	if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
		glfwSetWindowShouldClose(window, true);
}

unsigned int Lab02Application::Run() const
{
	float triangle[2 * 4] = {
		0.5f, 0.5f,
		0.5f, -0.5f,
		-0.5f, -0.5f,
		-0.5f, 0.5f
	};

	unsigned int triangleTopology[3] = {
		2, 3, 0
	};

	//GLuint vao;
	/*glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);

	VertexBuffer vbo(triangle, sizeof(triangle));
	IndexBuffer ebo(triangleTopology, 3);

	glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, sizeof(float) * 2, (void*)0);
	glEnableVertexAttribArray(0);*/

	// Create buffers and arrays
	auto vao = std::make_shared<VertexArray>();
	auto vbo = std::make_shared<VertexBuffer>(triangle, sizeof(triangle));
	auto bufferLayout = BufferLayout({ {ShaderDataType::Float2,"position"} });
	vbo->SetLayout(bufferLayout);
	vao->AddVertexBuffer(vbo);
	auto ebo = std::make_shared<IndexBuffer>(triangleTopology, 3);
	vao->SetIndexBuffer(ebo);

	ShaderProgramSource shaderSources = ParseShader("../../../../Labs/Lab02/Other02/Basic.shader");

	auto shader = std::make_shared<Shader>(shaderSources.VertexSource, shaderSources.FragmentSource);

	while (!glfwWindowShouldClose(window))
	{

		glClearColor(164.0f / 256.0f, 96.0f / 256.0f, 237.0f / 256.0f, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT);

		shader->Bind();
		vao->Bind();
		glDrawElements(GL_TRIANGLES, 3, GL_UNSIGNED_INT, 0);

		glfwPollEvents();
		glfwSwapBuffers(window);
		processInput(window);
	}

	glfwDestroyWindow(window);
	glfwTerminate();

	return EXIT_SUCCESS;
}
