#pragma once
#include <GLFWApplication.h>

class Lab02Application : public GLFWApplication {
	bool ifTriangle, ifSquare;
public:
	Lab02Application(const std::string& name, const std::string& version);
	~Lab02Application();

	unsigned int ParseArguments(int argc, char** argv) override;
	// Run function
	unsigned int Run() const override;
};
