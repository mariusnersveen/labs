#include <iostream>
#include <tclap/CmdLine.h>
#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <fstream>
#include <sstream>
#include "Lab02Application.h"
#include <GeometricTools.h>


// Error checking
#define ASSERT(x) if (!(x)) __debugbreak();
#define GLCall(x) GLClearError();\
    x;\
    ASSERT(GLLogCall(#x, __FILE__, __LINE__))

static void GLClearError()
{
	while (glGetError() != GL_NO_ERROR);
}

static bool GLLogCall(const char* function, const char* file, int line)
{
	while (GLenum error = glGetError())
	{
		std::cout << "[OpenGL Error] (" << error << ")" << function << " " << file << ":" << line << std::endl;
		return false;
	}
	return true;
}


Lab02Application::Lab02Application(const std::string& name_, const std::string& version_)
	: GLFWApplication(name_, version_), ifTriangle(false), ifSquare(false) {}

Lab02Application::~Lab02Application() {}

unsigned int Lab02Application::ParseArguments(int argc, char** argv)
{

	try {
		// cmd(printed last in the help text, delimiter, version number, disable default arguments)
		TCLAP::CmdLine cmd("Command description message", ' ', "0.9", false);

		// SwitchArg is a boolean. (flag, name, description, add to command line object, default value)
		TCLAP::SwitchArg consoleSwitch("c", "console", "print \": console\"", cmd, false);
		TCLAP::SwitchArg triangleSwitch("t", "triangle", "print triangle", cmd, false);
		TCLAP::SwitchArg squareSwitch("s", "square", "print square", cmd, false);

		TCLAP::ValueArg<int> widthArg("w", "width", "configure the width of the window", false, 600, "int");
		TCLAP::ValueArg<int> heightArg("h", "height", "configure the height of the window", false, 600, "int");

		cmd.add(widthArg);
		cmd.add(heightArg);

		// Parse the argv array.
		cmd.parse(argc, argv);

		ifTriangle = triangleSwitch.getValue();
		ifSquare = squareSwitch.getValue();
		width = widthArg.getValue();
		height = heightArg.getValue();

	}
	catch (TCLAP::ArgException& e) {
		std::cerr << "error: " << e.error() << " for arg " << e.argId() << std::endl;
		return 1;
	}
	return 0;
}

struct ShaderProgramSource
{
	std::string VertexSource;
	std::string FragmentSource;
};

static ShaderProgramSource ParseShader(const std::string& filepath)
{
	std::ifstream stream(filepath);

	enum class ShaderType { NONE = -1, VERTEX = 0, FRAGMENT = 1 };

	std::string line;
	std::stringstream ss[2];
	ShaderType type = ShaderType::NONE;
	while (getline(stream, line)) {
		if (line.find("#shader") != std::string::npos) {
			if (line.find("vertex") != std::string::npos) {
				type = ShaderType::VERTEX;
			}
			else if (line.find("fragment") != std::string::npos) {
				type = ShaderType::FRAGMENT;
			}
		}
		else {
			ss[(int)type] << line << "\n";
		}
	}

	return { ss[0].str(), ss[1].str() };
}

static unsigned int CreateShader(const std::string& vertexShaderSrc, const std::string& fragmentShaderSrc)
{
	unsigned int shaderProgram;
	GLCall(shaderProgram = glCreateProgram());

	// compile the vertex shader
	unsigned int vertexShader;
	GLCall(vertexShader = glCreateShader(GL_VERTEX_SHADER));
	GLCall(const GLchar * vss = vertexShaderSrc.c_str());
	GLCall(glShaderSource(vertexShader, 1, &vss, NULL));
	GLCall(glCompileShader(vertexShader));
	// check for shader compile errors
	int success;
	char infoLog[512];
	glGetShaderiv(vertexShader, GL_COMPILE_STATUS, &success);
	if (!success) {
		glGetShaderInfoLog(vertexShader, 512, NULL, infoLog);
		std::cout << "ERROR::SHADER::VERTEX::COMPILATION_FAILED\n" << infoLog << std::endl;
	}

	// compile the fragment shader
	unsigned int fragmentShader;
	fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
	const GLchar* fss = fragmentShaderSrc.c_str();
	GLCall(glShaderSource(fragmentShader, 1, &fss, NULL));
	GLCall(glCompileShader(fragmentShader));
	// check for shader compile errors
	GLCall(glGetShaderiv(vertexShader, GL_COMPILE_STATUS, &success));
	if (!success) {
		glGetShaderInfoLog(vertexShader, 512, NULL, infoLog);
		std::cout << "ERROR::SHADER::FRAGMENT::COMPILATION_FAILED\n" << infoLog << std::endl;
	}

	// create a shader program
	GLCall(glAttachShader(shaderProgram, vertexShader));
	GLCall(glAttachShader(shaderProgram, fragmentShader));
	GLCall(glLinkProgram(shaderProgram));

	GLCall(glDeleteShader(vertexShader));
	GLCall(glDeleteShader(fragmentShader));

	return shaderProgram;
}

static void processInput(GLFWwindow* window)
{
	if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
		glfwSetWindowShouldClose(window, true);
}

static unsigned int run(GLFWwindow* window, bool ifTriangle, bool ifSquare)
{
	float currentTime, sinTime, cosTime;

	float triangleColor[3 * 3] = {
		0.0f, 0.0f, 0.0f,
		0.0f, 0.0f, 0.0f,
		0.0f, 0.0f, 0.0f
	};

	float squareColor[3 * 3 * 2] = {
		0.0f, 0.0f, 0.0f,
		0.0f, 0.0f, 0.0f,
		0.0f, 0.0f, 0.0f,
		
		0.0f, 0.0f, 0.0f,
		0.0f, 0.0f, 0.0f,
		0.0f, 0.0f, 0.0f,
	};


	// Create a vertex array (VAO)
	// VAO stores all of the state needed to supply vertex data
	GLuint vertexArrayId;
	glGenVertexArrays(1, &vertexArrayId);
	glBindVertexArray(vertexArrayId);   // set active VAO

	// Create vertex buffers (VBO)
	// VBO is a buffer used to transfer the	data from CPU to GPU
	GLuint vertexBufferId[2];
	glGenBuffers(2, vertexBufferId);



	if (ifTriangle)
	{
		// set active VBO (type of buffer, ID of the target buffer)
		glBindBuffer(GL_ARRAY_BUFFER, vertexBufferId[0]);
		glBufferData(GL_ARRAY_BUFFER, sizeof(GeometricTools::UnitTriangle2D), &GeometricTools::UnitTriangle2D, GL_STATIC_DRAW);
		glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, sizeof(float) * 2, (void*)0);
		glEnableVertexAttribArray(0);

		// set active VBO (type of buffer, ID of the target buffer)
		glBindBuffer(GL_ARRAY_BUFFER, vertexBufferId[1]);
		glBufferData(GL_ARRAY_BUFFER, sizeof(triangleColor), triangleColor, GL_DYNAMIC_DRAW);
		glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(float) * 3, (void*)0);
		glEnableVertexAttribArray(1);

	}

	if (ifSquare)
	{
		// set active VBO (type of buffer, ID of the target buffer)
		glBindBuffer(GL_ARRAY_BUFFER, vertexBufferId[0]);
		glBufferData(GL_ARRAY_BUFFER, sizeof(GeometricTools::UnitSquare2D), &GeometricTools::UnitSquare2D, GL_STATIC_DRAW);
		glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, sizeof(float) * 2, (void*)0);
		glEnableVertexAttribArray(0);

		// set active VBO (type of buffer, ID of the target buffer)
		glBindBuffer(GL_ARRAY_BUFFER, vertexBufferId[1]);
		glBufferData(GL_ARRAY_BUFFER, sizeof(squareColor), squareColor, GL_DYNAMIC_DRAW);
		glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(float) * 3, (void*)0);
		glEnableVertexAttribArray(1);
	}


	ShaderProgramSource shaderSources = ParseShader("../../../../Labs/Lab02/Other/Basic.shader");

	unsigned int shaderProgram = CreateShader(shaderSources.VertexSource, shaderSources.FragmentSource);
	GLCall(glUseProgram(shaderProgram));

	while (!glfwWindowShouldClose(window))
	{
		currentTime = glfwGetTime();
		sinTime = sin(currentTime) / 2;
		cosTime = cos(currentTime) / 2;

		GLCall(glClearColor(164.0f / 256.0f, 96.0f / 256.0f, 237.0f / 256.0f, 1.0f));
		GLCall(glClear(GL_COLOR_BUFFER_BIT));

		if (ifTriangle) {
			
			float newColor1[3] = { 135.0f / 256.0f + sinTime, 162.0f / 256.0f + sinTime, 251.0f / 256.0f + sinTime };
			float newColor2[3] = { 135.0f / 256.0f + cosTime, 162.0f / 256.0f + cosTime, 251.0f / 256.0f + cosTime };
			float newColor3[3] = { 173.0f / 256.0f + sinTime, 221.0f / 256.0f + sinTime, 208.0f / 256.0f + sinTime };

			GLCall(glBindBuffer(GL_ARRAY_BUFFER, vertexBufferId[1]));
			GLCall(glBufferSubData(GL_ARRAY_BUFFER, 0, 12, newColor1));
			GLCall(glBufferSubData(GL_ARRAY_BUFFER, 12, 12, newColor2));
			GLCall(glBufferSubData(GL_ARRAY_BUFFER, 24, 12, newColor3));
			
			GLCall(glBindBuffer(GL_ARRAY_BUFFER, vertexBufferId[0]));

			// Issue the draw call (primitive type, first element in buffer, number of elements to render)
			GLCall(glDrawArrays(GL_TRIANGLES, 0, 3));
		}
		if (ifSquare) {
			float newColor1[3] = { 0.1 + sinTime, 0, 0.3 + sinTime };
			float newColor2[3] = { 0.5 + cosTime, 0.5 + cosTime, 0.5 + cosTime };
			float newColor3[3] = { 0.5 + sinTime, 0.4 + sinTime, 0.3 + sinTime };
			float newColor4[3] = { 0.5 + sinTime, 0.5 + sinTime, 0.5 + sinTime };

			GLCall(glBindBuffer(GL_ARRAY_BUFFER, vertexBufferId[1]));
			GLCall(glBufferSubData(GL_ARRAY_BUFFER, 0, 12, newColor1));
			GLCall(glBufferSubData(GL_ARRAY_BUFFER, 12, 12, newColor2));
			GLCall(glBufferSubData(GL_ARRAY_BUFFER, 24, 12, newColor3));
			GLCall(glBufferSubData(GL_ARRAY_BUFFER, 36, 12, newColor1));
			GLCall(glBufferSubData(GL_ARRAY_BUFFER, 48, 12, newColor2));
			GLCall(glBufferSubData(GL_ARRAY_BUFFER, 60, 12, newColor3));

			GLCall(glBindBuffer(GL_ARRAY_BUFFER, vertexBufferId[0]));

			// Issue the draw call (primitive type, first element in buffer, number of elements to render)
			GLCall(glDrawArrays(GL_TRIANGLES, 0, 6));
		}



		GLCall(glfwPollEvents());
		GLCall(glfwSwapBuffers(window));
		GLCall(processInput(window));
	}







	glfwDestroyWindow(window);
	glfwTerminate();

	return EXIT_SUCCESS;
}

unsigned int Lab02Application::Run() const
{
	return run(window, ifTriangle, ifSquare);
}
