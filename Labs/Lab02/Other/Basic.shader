#shader vertex
#version 460 core

layout(location = 0) in vec2 position;
layout(location = 1) in vec3 color;
out vec3 vertexColor;

void main()
{
	// gl_PointSize = 10.0;
	gl_Position = vec4(position, 0.0, 1.0);
	vertexColor = color;
}

#shader fragment
#version 460 core

out vec4 fragColor;
in vec3 vertexColor;

void main()
{
	fragColor = vec4(vertexColor, 1.0);
}
