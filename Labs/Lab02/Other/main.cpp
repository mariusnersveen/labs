#include "Lab02Application.h"

int main(int argc, char** argv) {
	Lab02Application application("Lab02", "1.0");

	application.ParseArguments(argc, argv);
	application.Init();
	return application.Run();
}
