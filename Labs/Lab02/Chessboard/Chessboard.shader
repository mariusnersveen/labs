#shader vertex
#version 460 core

layout(location = 0) in vec2 position;

void main()
{
	// Hva gj�r denne?
	// gl_PointSize = 10.0;
	gl_Position = vec4(position, 0.0, 1.0);
}

#shader fragment
#version 460 core

out vec4 color;
uniform vec4 fragColor;

void main()
{
	color = fragColor;
}
