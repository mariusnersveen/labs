#include "Lab02Chessboard.h"

int main(int argc, char** argv) {
	Lab02Chessboard application("lab02Chessboard", "1.0");

	application.ParseArguments(argc, argv);
	application.Init();
	return application.Run();
}
