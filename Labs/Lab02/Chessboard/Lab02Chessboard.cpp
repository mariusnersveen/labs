#include <iostream>
#include <tclap/CmdLine.h>
#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <fstream>
#include <sstream>
#include "Lab02Chessboard.h"
#include <GeometricTools.h>


// Error checking
#define ASSERT(x) if (!(x)) __debugbreak();
#define GLCall(x) GLClearError();\
    x;\
    ASSERT(GLLogCall(#x, __FILE__, __LINE__))

static void GLClearError()
{
	while (glGetError() != GL_NO_ERROR);
}

static bool GLLogCall(const char* function, const char* file, int line)
{
	while (GLenum error = glGetError())
	{
		std::cout << "[OpenGL Error] (" << error << ")" << function << " " << file << ":" << line << std::endl;
		return false;
	}
	return true;
}


Lab02Chessboard::Lab02Chessboard(const std::string& name_, const std::string& version_)
	: GLFWApplication(name_, version_) {}

Lab02Chessboard::~Lab02Chessboard() {}

struct ShaderProgramSource
{
	std::string VertexSource;
	std::string FragmentSource;
};

static ShaderProgramSource ParseShader(const std::string& filepath)
{
	std::ifstream stream(filepath);

	enum class ShaderType { NONE = -1, VERTEX = 0, FRAGMENT = 1 };

	std::string line;
	std::stringstream ss[2];
	ShaderType type = ShaderType::NONE;
	while (getline(stream, line)) {
		if (line.find("#shader") != std::string::npos) {
			if (line.find("vertex") != std::string::npos) {
				type = ShaderType::VERTEX;
			}
			else if (line.find("fragment") != std::string::npos) {
				type = ShaderType::FRAGMENT;
			}
		}
		else {
			ss[(int)type] << line << "\n";
		}
	}

	return { ss[0].str(), ss[1].str() };
}

static unsigned int CreateShader(const std::string& vertexShaderSrc, const std::string& fragmentShaderSrc)
{
	unsigned int shaderProgram;
	shaderProgram = glCreateProgram();

	// compile the vertex shader
	unsigned int vertexShader;
	vertexShader = glCreateShader(GL_VERTEX_SHADER);
	const GLchar* vss = vertexShaderSrc.c_str();
	GLCall(glShaderSource(vertexShader, 1, &vss, NULL));
	GLCall(glCompileShader(vertexShader));
	// check for shader compile errors
	int success;
	char infoLog[512];
	GLCall(glGetShaderiv(vertexShader, GL_COMPILE_STATUS, &success));
	if (!success) {
		GLCall(glGetShaderInfoLog(vertexShader, 512, NULL, infoLog));
		std::cout << "ERROR::SHADER::VERTEX::COMPILATION_FAILED\n" << infoLog << std::endl;
	}

	// compile the fragment shader
	unsigned int fragmentShader;
	GLCall(fragmentShader = glCreateShader(GL_FRAGMENT_SHADER));
	const GLchar* fss = fragmentShaderSrc.c_str();
	GLCall(glShaderSource(fragmentShader, 1, &fss, NULL));
	GLCall(glCompileShader(fragmentShader));
	// check for shader compile errors
	GLCall(glGetShaderiv(vertexShader, GL_COMPILE_STATUS, &success));
	if (!success) {
		GLCall(glGetShaderInfoLog(vertexShader, 512, NULL, infoLog));
		std::cout << "ERROR::SHADER::FRAGMENT::COMPILATION_FAILED\n" << infoLog << std::endl;
	}

	// create a shader program
	GLCall(glAttachShader(shaderProgram, vertexShader));
	GLCall(glAttachShader(shaderProgram, fragmentShader));
	GLCall(glLinkProgram(shaderProgram));

	GLCall(glDeleteShader(vertexShader));
	GLCall(glDeleteShader(fragmentShader));

	return shaderProgram;
}

static void processInput(GLFWwindow* window)
{
	if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
		glfwSetWindowShouldClose(window, true);
}

template <int X> static void processArrowInput(GLFWwindow* window, std::array<float, X>& object)
{
	float size = (float)1 / 8;
	// right
	if (glfwGetKey(window, GLFW_KEY_RIGHT) == GLFW_PRESS && KeyState::right == false) {
		KeyState::right = true;
		for (int i = 0; i < object.size(); i += 2) {
			object[i] += size;
		}
	}
	else if (glfwGetKey(window, GLFW_KEY_RIGHT) == GLFW_RELEASE && KeyState::right == true) {
		KeyState::right = false;
	}
	// left
	if (glfwGetKey(window, GLFW_KEY_LEFT) == GLFW_PRESS && KeyState::left == false) {
		KeyState::left = true;
		for (int i = 0; i < object.size(); i += 2) {
			object[i] -= size;
		}
	}
	else if (glfwGetKey(window, GLFW_KEY_LEFT) == GLFW_RELEASE && KeyState::left == true) {
		KeyState::left = false;
	}
	// up
	if (glfwGetKey(window, GLFW_KEY_UP) == GLFW_PRESS && KeyState::up == false) {
		KeyState::up = true;
		for (int i = 0; i < object.size(); i += 2) {
			object[i + 1] += size;
		}
	}
	else if (glfwGetKey(window, GLFW_KEY_UP) == GLFW_RELEASE && KeyState::up == true) {
		KeyState::up = false;
	}
	// down
	if (glfwGetKey(window, GLFW_KEY_DOWN) == GLFW_PRESS && KeyState::down == false) {
		KeyState::down = true;
		for (int i = 0; i < object.size(); i += 2) {
			object[i + 1] -= size;
		}
	}
	else if (glfwGetKey(window, GLFW_KEY_DOWN) == GLFW_RELEASE && KeyState::down == true) {
		KeyState::down = false;
	}
}

namespace KeyState
{
	bool right = false;
	bool left = false;
	bool up = false;
	bool down = false;
}

static void keyCallback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
	if (key == GLFW_KEY_SPACE && action == GLFW_PRESS)
		std::cout << "space is pressed" << std::endl;
}

static unsigned int run(GLFWwindow* window)
{
	glfwSetKeyCallback(window, keyCallback);
	GLuint vertexArrayId[2];
	GLCall(glGenVertexArrays(2, vertexArrayId));

	GLuint vertexBufferId[2];
	GLCall(glGenBuffers(2, vertexBufferId));

	GLuint elementBufferId[2];
	GLCall(glGenBuffers(2, elementBufferId));

	// bind vertex array object
	GLCall(glBindVertexArray(vertexArrayId[0]));
	// copy vertices array in a vertex buffer for OpenGL to use
	GLCall(glBindBuffer(GL_ARRAY_BUFFER, vertexBufferId[0]));
	auto grid = GeometricTools::UnitGrid2D<9, 9>();
	GLCall(glBufferData(GL_ARRAY_BUFFER, sizeof(grid.grids), &grid.grids, GL_STATIC_DRAW));
	// copy index array in a element buffer for OpenGL to use
	auto check1 = grid.UnitGridTopologyCheck1();
	GLCall(glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, elementBufferId[0]));
	GLCall(glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(check1), &check1, GL_STATIC_DRAW));
	auto check2 = grid.UnitGridTopologyCheck2();
	GLCall(glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, elementBufferId[1]));
	GLCall(glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(check2), &check2, GL_STATIC_DRAW));
	// set the vertex attributes pointers
	GLCall(glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, sizeof(float) * 2, (void*)0));
	GLCall(glEnableVertexAttribArray(0));

	GLCall(glBindVertexArray(vertexArrayId[1]));
	GLCall(glBindBuffer(GL_ARRAY_BUFFER, vertexBufferId[1]));
	auto square = GeometricTools::UnitSquare2D;
	for (int i = 0; i < square.size(); i++)
		square[i] /= (float)8;
	float gap_to_top_left = square[0] + 0.5;
	for (int i = 0; i < square.size(); i += 2) {
		square[i] -= gap_to_top_left;
		square[i + 1] += gap_to_top_left;
	}
	GLCall(glBufferData(GL_ARRAY_BUFFER, sizeof(square), &square, GL_DYNAMIC_DRAW));
	GLCall(glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, sizeof(float) * 2, (void*)0));
	GLCall(glEnableVertexAttribArray(0));

	GLCall(ShaderProgramSource shaderSources = ParseShader("../../../../Labs/Lab02/Chessboard/Chessboard.shader"));

	GLCall(unsigned int shaderProgram = CreateShader(shaderSources.VertexSource, shaderSources.FragmentSource));

	while (!glfwWindowShouldClose(window))
	{
		GLCall(glClearColor(164.0f / 256.0f, 96.0f / 256.0f, 237.0f / 256.0f, 1.0f));
		GLCall(glClear(GL_COLOR_BUFFER_BIT));

		GLCall(glUseProgram(shaderProgram));
		GLCall(int vertexColorLocation = glGetUniformLocation(shaderProgram, "fragColor"));
		GLCall(glUniform4f(vertexColorLocation, 0.0, 0.0, 0.0, 0.0));
		GLCall(glBindVertexArray(vertexArrayId[0]));
		GLCall(glBindBuffer(GL_ARRAY_BUFFER, vertexBufferId[0]));
		GLCall(glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, elementBufferId[0]));
		GLCall(glDrawElements(GL_TRIANGLES, 64 * 3, GL_UNSIGNED_INT, 0));

		GLCall(glUniform4f(vertexColorLocation, 1.0, 1.0, 1.0, 1.0));
		GLCall(glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, elementBufferId[1]));
		GLCall(glDrawElements(GL_TRIANGLES, 64 * 3, GL_UNSIGNED_INT, 0));

		processArrowInput(window, square);
		GLCall(glUniform4f(vertexColorLocation, 0.0, 1.0, 0.0, 1.0));
		GLCall(glBindVertexArray(vertexArrayId[1]));
		GLCall(glBindBuffer(GL_ARRAY_BUFFER, vertexBufferId[1]));
		GLCall(glBufferSubData(GL_ARRAY_BUFFER, 0, 48, &square));
		GLCall(glDrawArrays(GL_TRIANGLES, 0, 6));

		GLCall(glfwPollEvents());
		GLCall(glfwSwapBuffers(window));
		processInput(window);
	}

	std::cout << "final function---------------------------------" << std::endl;
	for (int i = 0; i < square.size(); i += 2) {
		std::cout << i << ":" << square[i] << ", ";
		std::cout << square[i + 1] << std::endl;
	}

	glfwDestroyWindow(window);
	glfwTerminate();

	return EXIT_SUCCESS;
}

unsigned int Lab02Chessboard::Run() const
{
	return run(window);
}
