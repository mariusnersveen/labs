#pragma once
#include <GLFWApplication.h>

class Lab02Chessboard : public GLFWApplication {
public:
	Lab02Chessboard(const std::string& name, const std::string& version);
	~Lab02Chessboard();
	// Run function
	unsigned int Run() const override;
};